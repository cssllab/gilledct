#include "finding.h"
#include "const.h"
//#include "functions.h"
#include "div_up.h"
#include "d_rng.cuh"


//#define EXTRA_OUTPUT

template <typename T> __device__ __forceinline__
int FirstThreadIdThatGE(T threadVal, T toFind)
{
	int predicate=threadVal>=toFind;
	unsigned int blt=__ballot(predicate);
	return __ffs(blt)-1;
}

__device__ inline
int WarpChunks(int len)//TODO: move to separate file
{
	assert(warpSize==32);
	return (len&(31))?((len>>5)+1):(len>>5);
}

//needs all 32 threads in a warp to run this function
//val is an input value for the current thread
//takes extra array of 64 elements in a shared memory to perform summatiom
template <typename T>
__forceinline__
__device__ T inclusive_warp_scan(int idx, T val, volatile T *s_data)
{
	assert(idx<warpSize);
	assert(s_data[idx]==(T)0);
	
//	s_data[idx]=0.;
	idx+=warpSize;
	assert(idx<warpSize*2);
	T t=s_data[idx]=val;
	
#ifndef NDEBUG
	if(idx==32){
		for(int i=0; i<warpSize; ++i){
			if(s_data[i]!=0.)
				printf("error in inclusive_warp_scan: must be zeros %d, %e\n", i, s_data[i]);
		}
	}
#endif

	s_data[idx]=t=t+s_data[idx-1];
	s_data[idx]=t=t+s_data[idx-2];
	s_data[idx]=t=t+s_data[idx-4];
	s_data[idx]=t=t+s_data[idx-8];
	s_data[idx]=t=t+s_data[idx-16];
	return s_data[idx];
}

//function detects if a requested partial sum ("val2Search") is contained within
//a chunk of array "arr" that has "warpsInChunk"*warpSize length
//"add" value accumluates partial sum from previous chunks
//total accumulated value is stored in the last element of the auxilary array, "s_aux_for_scan[63]"
__device__ __forceinline__
bool IsFoundInChunk(int inWarpIdx, PropReal_t const *arr, PropReal_t val2Search, int warpsInChunk, volatile PropReal_t *s_aux_for_scan, PropReal_t &add)
{
	add=s_aux_for_scan[63];
	volatile PropReal_t threadSum=0;

	//every thread in a warp accumulates values with a warpSize stride
	for(int i=0; i<warpsInChunk; ++i){
		const int ind=warpSize*i+inWarpIdx;
		volatile PropReal_t val1=arr[ind];
		threadSum+=val1;
	}

	//partial sum for a thread, also accumulated in s_aux_for_scan[32+inWarpIdx]
	PropReal_t scan=inclusive_warp_scan(inWarpIdx, threadSum, s_aux_for_scan);
	
	if(inWarpIdx==0)
		s_aux_for_scan[63]+=add;

	return s_aux_for_scan[63]>=val2Search;
}

template <typename T>
__device__ __forceinline__
int FindInArray(int inWarpIdx, int warpInBlockIdx, T val2Search, PropReal_t addition, PropReal_t const *arr, int arrLen, T volatile *s_aux_for_scan){
	int const chunks=WarpChunks(arrLen);
		
	__shared__ T add[WARPS_IN_BLOCK];

	if(inWarpIdx==31){
		add[warpInBlockIdx]=0.f;
	}

	s_aux_for_scan[inWarpIdx]=0.;

	for(int ch=0; ch<chunks; ++ch){
		T prpWarpSum=inclusive_warp_scan(inWarpIdx, arr[ch*warpSize+inWarpIdx], s_aux_for_scan);
			
		int foundInChunkInd=FirstThreadIdThatGE(prpWarpSum+add[warpInBlockIdx]+addition, val2Search);//all threads in a warp get the same value

		if(foundInChunkInd>=0){
			return ch*warpSize+foundInChunkInd;//all threads return the same value
		}

		if(inWarpIdx==31){
			add[warpInBlockIdx]+=prpWarpSum;
		}
	}
	return -1;
}

template <bool storeValue2Find>
__global__ // __launch_bounds__(256)
void FindReactionToBeFiredKernel(int realizationsCount, int chsCount, 
							   uint4 *rSeeds, //seeds for random reaction selection
							   CudaPitched2D<PropReal_t> propensities, //chsCount x realizationsCount
							   PropSumReal_t const *totalPropSum, //rangesInArray x realizationsCount
							   int predictedIndex, //length of searching chunk
							   int const *rangesIndices, int rangesInArray,  //subrange ends' indices, subranges count
							   int *reactionsToFire, //what reaction to fire
							   PropReal_t *val2Find//not used when storeValue2Find==false
							 )
{

	int const idx=blockDim.x * blockIdx.x + threadIdx.x;
	int const realizationIdx=idx>>5;

	if(realizationIdx>=realizationsCount)
		return;

	__shared__ PropReal_t s_val2Search[WARPS_IN_BLOCK];

	int const inWarpIdx=idx&31;
	
	int const warpInBlockIdx=realizationIdx%WARPS_IN_BLOCK;

	uint4 seed;
	if(inWarpIdx==0){
		seed=rSeeds[realizationIdx];
		PropReal_t rn=HybridTausRngReduced(&seed);
		s_val2Search[warpInBlockIdx]=(PropReal_t)(rn*totalPropSum[realizationIdx*rangesInArray+rangesInArray-1]);
		if(storeValue2Find)
			val2Find[realizationIdx]=s_val2Search[warpInBlockIdx];
	}

	//detect subrange that the reaction to be fired belongs to
	PropReal_t rangeBound;
	if(inWarpIdx<rangesInArray)
		rangeBound=(PropReal_t)totalPropSum[realizationIdx*rangesInArray+inWarpIdx];
	else
		rangeBound=1E+30;

	int range=FirstThreadIdThatGE(rangeBound, s_val2Search[warpInBlockIdx])-1;

	int warpsInChunkCount=DIV_UP(predictedIndex, warpSize);

	__shared__ PropReal_t s_aux_for_scan[64*WARPS_IN_BLOCK];//auxilary shared array for finding per-warp prefix sum
	
	(s_aux_for_scan+64*warpInBlockIdx)[inWarpIdx]=0.;

	int elemInChunk=warpsInChunkCount*warpSize;
		
	PropReal_t add;
	int shift;
	if(range==-1){
		add=0.f;
		shift=0;
	}
	else{
		add=totalPropSum[realizationIdx*rangesInArray+range];//total partial sum for previous chunk
		shift=rangesIndices[range];
	}

	//float auxAdd=add;//remove!

	if(inWarpIdx==31)
		(s_aux_for_scan+64*warpInBlockIdx)[63]=add;//use the last element for storing total partial sum for previous chunk

	int maxIt=DIV_UP(chsCount, warpsInChunkCount*warpSize);
	int i;

	assert(add<=s_val2Search[warpInBlockIdx]);
	//assert(totalPropSum[realizationIdx*rangesInArray+range+1]>s_val2Search[warpInBlockIdx]);
	for(i=0; i<maxIt; ++i){
		if(i==maxIt-1)
			warpsInChunkCount=DIV_UP(chsCount-i*warpsInChunkCount*warpSize,warpSize);

		if(IsFoundInChunk(inWarpIdx, propensities.Row(realizationIdx)+shift+elemInChunk*i, s_val2Search[warpInBlockIdx],  warpsInChunkCount, s_aux_for_scan+64*warpInBlockIdx, add))
			break;
	}

	if(i>=maxIt&&inWarpIdx==0){
		assert((s_aux_for_scan+64*warpInBlockIdx)[63]<s_val2Search[warpInBlockIdx]);
#ifdef EXTRA_OUTPUT
		printf("realization %d tried to find %e, total sum: %e, exiting with -1\n", 
			realizationIdx, s_val2Search[warpInBlockIdx], totalPropSum[realizationIdx*rangesInArray+rangesInArray-1]);
#endif
		reactionsToFire[realizationIdx]=-1;
		rSeeds[realizationIdx]=seed;
		return;
	}

	int foundPos=FindInArray(inWarpIdx, warpInBlockIdx, s_val2Search[warpInBlockIdx], add, propensities.Row(realizationIdx)+shift+elemInChunk*i, chsCount-(shift+elemInChunk*i), s_aux_for_scan+warpInBlockIdx*64);

	if(inWarpIdx==0){
		rSeeds[realizationIdx]=seed;
		reactionsToFire[realizationIdx]=foundPos+shift+elemInChunk*i;
	}
}


void FindReactionToBeFired(int realizationsCount, int chsCount, 
							   uint4 *rSeeds, //seeds for random reaction selection
							   CudaPitched2D<PropReal_t> propensities, //chsCount x realizationsCount
							   PropSumReal_t const *totalPropSum, //rangesInArray x realizationsCount
							   int predictedIndex, //length of searching chunk
							   int const *rangesIndices, int rangesInArray, //subrange ends' indices, subranges count
							   int *reactionsToFire, //what reaction to fire
							   PropReal_t *val2Find,//not used when storeValue2Find==false
							   bool storeValuesToFind//store or not values to find
							   )
{
	int const BLOCK_SIZE=32*WARPS_IN_BLOCK;
	int const BLOCKS=DIV_UP(realizationsCount, WARPS_IN_BLOCK);

	if(storeValuesToFind)
		FindReactionToBeFiredKernel<true><<<BLOCKS, BLOCK_SIZE>>>(realizationsCount, chsCount, 
			rSeeds, 
			propensities, totalPropSum,
			predictedIndex,
			rangesIndices, rangesInArray,
			reactionsToFire, 
			val2Find);
	else
		FindReactionToBeFiredKernel<false><<<BLOCKS, BLOCK_SIZE>>>(realizationsCount, chsCount, 
			rSeeds, 
			propensities, totalPropSum,
			predictedIndex,
			rangesIndices, rangesInArray,
			reactionsToFire, 
			val2Find);

}