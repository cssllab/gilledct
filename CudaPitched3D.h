#pragma once
#include <cassert>
#include <stdexcept>
#include "cuda_wrap.h"
#include "CudaPitched2D.h"
#include "defines.h"

template <typename T>
class CudaPitched3D
{

	cudaPitchedPtr m_devPitchedPtr;

	cudaExtent m_ext;

	//size_t pitch = devPitchedPtr.pitch; size_t slicePitch = pitch * height;
	int m_slicePitch;

public:
	__host__
	CudaPitched3D()
	{
		m_devPitchedPtr.ptr=NULL;
		m_devPitchedPtr.pitch=0;
		m_devPitchedPtr.xsize=0;
		m_devPitchedPtr.ysize=0;

		m_ext.width=0;
		m_ext.height=0;
		m_ext.depth=0;

	}

	__host__
	void Init(int width, int height, int depth)
	{
		cudaExtent ext=make_cudaExtent(width * sizeof(T), height, depth);
		CHECK_CUDA_ERRORS();
		if(cudaMalloc3D(&m_devPitchedPtr, ext)!=cudaSuccess)
			throw std::runtime_error("can not allocate a pitched array");
		m_slicePitch=m_devPitchedPtr.pitch*height;
		m_ext=ext;
		CHECK_CUDA_ERRORS();

		cudaMemset3D(m_devPitchedPtr, 0, 
			m_ext);
		CHECK_CUDA_ERRORS();
	}


	__host__ __device__
	T *Row(int r, int sliceN)
	{
		assert(r<m_ext.height);
		assert(sliceN<m_ext.depth);

		if(sliceN>=m_ext.depth)
			printf("sliceN=%d, depth=%d\n",sliceN, m_ext.depth);

		char* devPtr = (char*)m_devPitchedPtr.ptr;

		char* slice = devPtr + sliceN * m_slicePitch;

		return (T *)(slice + r * m_devPitchedPtr.pitch);
	}


	__host__ __device__
	T const *Row(int r, int sliceN)const
	{
		assert(r<(int)m_ext.height);
		assert(sliceN<(int)m_ext.depth);
		//if(sliceN>=(int)m_ext.depth)
		//	printf("sliceN=%d, depth=%d\n",sliceN, m_ext.depth);

		char* devPtr = (char*)m_devPitchedPtr.ptr;

		char* slice = devPtr + sliceN * m_slicePitch;

		return (T *)(slice + r * m_devPitchedPtr.pitch);
	}


	__host__ __device__
	std::size_t Height()const{return m_ext.height;}

	__host__ __device__
	std::size_t Depth()const{return m_ext.depth;}

	__host__ __device__
	std::size_t Width()const{return m_ext.width/sizeof(T);}//width in elements, not in bytes!

	__host__
	void CopyTo(CudaPitched3D &other)const
	{
		assert(other.m_ext==m_ext);
		cudaMemcpy3DParms p={0};
		p.srcPtr=this->m_devPitchedPtr;
		p.dstPtr=other.m_devPitchedPtr;
		p.extent=m_ext;
		p.kind=cudaMemcpyDeviceToDevice;
		cudaMemcpy3D(&p);
	}
};
