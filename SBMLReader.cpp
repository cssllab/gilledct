#include "SBMLReader.h"
#include "functions.h"

using std::vector;
using std::make_pair;
using std::string;
using std::unordered_map;
using rapidxml::xml_document;
using rapidxml::xml_node;
using rapidxml::xml_attribute;

SBMLReader::SBMLReader(void){}

SBMLReader::~SBMLReader(void){}

void SBMLReader::ReadXML(const char *fn, std::vector<ReactionProp> &reactionProps,
		ReactantsPopulations_t &reactantsPopulations)const{

	char err[250];
	if(!FileExists(fn))
	{
		sprintf(err, "Model::ReadXML: can not open file %s", fn);
		throw std::runtime_error(err);
	}

	vector<char> buff;
	FileToMem(fn, buff);
	
	xml_document<> doc;
	doc.parse<0>(&buff[0]);

	xml_node<> *sbmlNode = doc.first_node("sbml");
	if(sbmlNode==NULL){
		sprintf(err, "SBMLReader::ReadXML: can not find %s node", "sbml");
		throw std::runtime_error(err);
	}

	xml_node<> *modelNode = sbmlNode->first_node("model");
	if(modelNode==NULL){
		sprintf(err, "SBMLReader::ReadXML: can not find %s node", "model");
		throw std::runtime_error(err);
	}

	ReadSpecies(modelNode, reactantsPopulations);

	unordered_map<string, float> reactionParameters;
	ReadParameters(modelNode, reactionParameters);

	//without rates
//	ReadReactions();

	int qq=0;
}

void SBMLReader::ReadSpecies(rapidxml::xml_node<> *modelNode,
							  ReactantsPopulations_t &reactantsPopulations)const{
	xml_node<> *listOfSpeciesNode=FirstNode(modelNode, "listOfSpecies", true);

	xml_node<> *speciesNode=FirstNode(listOfSpeciesNode, "species", true);
	int i=0;
	do{
		const char *sName=speciesNode->first_attribute("id")->value();
		int population=atoi(speciesNode->first_attribute("initialConcentration")->value());
		reactantsPopulations[sName]=make_pair(i++, population);
	}while(speciesNode=speciesNode->next_sibling());
}

void SBMLReader::ReadParameters(xml_node<> *modelNode, 
		unordered_map<string, float> &reactionParameters)const{
	xml_node<> *listOfParametersNode=FirstNode(modelNode, "listOfParameters", true);

	xml_node<> *parameterNode=FirstNode(listOfParametersNode, "parameter", true);

	int i=0;
	do{
		const char *sName=parameterNode->first_attribute("id")->value();
		xml_attribute<> *populationAttr=parameterNode->first_attribute("value");
		if(populationAttr==NULL)
			continue;
		reactionParameters[sName]=atof(populationAttr->value());
	}while(parameterNode=parameterNode->next_sibling());
}