PROJECT(gilledct) 

CMAKE_MINIMUM_REQUIRED(VERSION 2.8.5)

FIND_PACKAGE(CUDA)

SET (CUDA_HOST_COMPILATION_CPP OFF)
set(CMAKE_CXX_FLAGS "-std=c++0x")
set(CUDA_NVCC_FLAGS -arch=sm_20)
SET(RAPIDXML CACHE PATH "Path to the folder that contains rapidxml directory")
SET(FLOAT_TYPE_STRING CACHE STRING "Specify 'float' or 'double' to be used")

IF(NOT FLOAT_TYPE_STRING STREQUAL "float" AND NOT FLOAT_TYPE_STRING STREQUAL "double")
 message( FATAL_ERROR "Only 'float' or 'double' can be used for FLOAT_TYPE_STRING")
ENDIF(NOT FLOAT_TYPE_STRING STREQUAL "float" AND NOT FLOAT_TYPE_STRING STREQUAL "double")

SET(RealTypeRequested ${FLOAT_TYPE_STRING})

# configure a header file to pass some of the CMake settings
# to the source code 
CONFIGURE_FILE (
	"${CMAKE_CURRENT_SOURCE_DIR}/common/realtype.h.in"
	"${CMAKE_CURRENT_SOURCE_DIR}/common/realtype.h"
)

INCLUDE_DIRECTORIES("${PROJECT_SOURCE_DIR}/common")
INCLUDE_DIRECTORIES(${RAPIDXML})

SET(HEADERS
	AffectedChannels.h
	aux_funcs.h
	control_vars.h
	CudaPitched2D.h
	CudaPitched3D.h
	cuda_wrap.h
	CummStat.h
	d_print.h
	finding.h
	Network.h
	propensities.h
	SBMLReader.h
	Statistics.h
	stdafx.h
	ThreadRanges.h
	thrust_functions.h
	../common/cl_parser.h
	../common/const.h
	../common/CSCHostMatrixData.h
	../common/CSCMatrix.h
	../common/CSRMatrix.h
	../common/CSRMatrixPacked.h
	../common/defines.h
	../common/div_up.h
	../common/FileReader.h
	../common/functions.h
	../common/LolCatReader.h
	../common/MP_funcs.h
	../common/MyTime.h
	../common/ReactionProp.h
	../common/realtype.h
	../common/StochKitReader.h
)
SET (CUDA_HEADERS
	AuxFunctors.cuh
	Loggers.cuh
	propfunctors.cuh
	../common/CSCHostMatrixData.cuh
	../common/defines.cuh
	../common/d_rng.cuh
	../common/MP_funcs.cuh
)

SET (CUDA_SOURCE
	propensities.cu 
	finding.cu
	Statistics.cu
	ThreadRanges.cu
	aux_funcs.cu
)

source_group("CUDA Headers" FILES ${CUDA_HEADERS})
source_group("CUDA Source" FILES ${CUDA_SOURCE})

#source_group("My Headers" FILES ${MY_HDRS})

CUDA_ADD_EXECUTABLE(gilledct 
control_vars.cpp
../common/functions.cpp
../common/StochKitReader.cpp 
../common/cl_parser.cpp
../common/CSCHostMatrixData.cpp  
gilledct.cpp
CummStat.cpp
${CUDA_SOURCE}
${HEADERS}
${CUDA_HEADERS})

