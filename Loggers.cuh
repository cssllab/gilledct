#pragma once

/*
void LogPropensitiesPrefixSum()
{
#ifdef LOG_ENABLED
	int chsCount=d_propensities.Width();
	int statesCount=d_reactantPopulation.Height();

	printf("Propensities' prefix sum\n");
	
	for(int i=0; i<statesCount; ++i)
	{
		cout<<DevArrStreamer(device_pointer_cast(d_prpsPrefixSum.Row(i)), chsCount+1)<<endl;
	}
#endif
}*/

void LogPopulation()
{
#ifdef LOG_ENABLED
	int rctsCount=d_reactantPopulation.Width();
	int statesCount=d_reactantPopulation.Height();

	printf("\nPopulations:\n");
	for(int i=0; i<statesCount; ++i)
	{
		cout<<"rlzn: "<<DevArrStreamer(device_pointer_cast(d_reactantPopulation.Row(i)), rctsCount)<<endl;
		//cout<<population<<endl;
		
	}
#endif
}

void LogReactantIndices(int rctsCount)
{
#ifdef LOG_ENABLED
	printf("\n\nReactants' index: \n");
	cout<<DevArrStreamer<int, int, Splitter>(
		device_pointer_cast(g_n.d_ChannelReactants),rctsCount,
		Splitter())<<endl;
	printf("\n");

#endif
}

void LogChannelBegins(int sz)
{
#ifdef LOG_ENABLED
	printf("\nChannel begins: \n");
    cout<<DevArrStreamer(device_pointer_cast(g_n.d_ChannelBegins), sz);
#endif
}


#ifdef ODM
void LogPropenseties()
{
#ifdef LOG_ENABLED
	int statesCount=d_reactantPopulation.Height();
	int chsCount=d_propensities.Width();

	printf("Propensities:\n");
	for(int i=0; i<statesCount; ++i)
	{
		cout<<"rlzn: "<<DevArrStreamer(device_pointer_cast(d_propensities.Row(i)), chsCount)<<endl;
	}
#endif
}
#endif

void LogSortedReactions(int chsCount)
{/*
#ifdef LOG_ENABLED
	printf("\nSorted reaction:");
	for(int i=0; i<4; ++i)
	{
		cout<<endl<<DevArrStreamer(device_pointer_cast(g_n.d_reactions_sorted), chsCount);
	}
#endif
	*/
}

void LogIndexing(int rctsCount)
{/*
#ifdef LOG_ENABLED
	printf("\nIndexing: \n");
	for(int i=0; i<4; ++i)
	{
		cout<<DevArrStreamer(device_pointer_cast(g_n.d_index_sorted)+(rctsCount+1)*i, rctsCount+1)<<
			endl;
	}
#endif*/
}

void LogChannelConstants(int chsCount)
{
#ifdef LOG_ENABLED
	printf("\nChannels' constants: \n");
	cout<<DevArrStreamer(device_pointer_cast(g_n.d_ChannelConstants), chsCount);
	printf("\n");
	CHECK_CUDA_ERRORS();
#endif
}

	
void LogChannelOrders(int chsCount)
{
#ifdef LOG_ENABLED
	printf("\nChannels' types: \n");
	cout<<DevArrStreamer<int, char>(device_pointer_cast(g_n.d_ChannelTypes), chsCount)<<endl;
#endif
}

void LogTimes()
{
#ifdef LOG_ENABLED
	printf("Current times:\n");
	int statesCount=d_reactantPopulation.Height();
	for(int i=0; i<statesCount; ++i)
	{
		PropReal_t tau=g_times[i];
		cout<<"tau["<<i<<"]="<<tau<<endl;
	}
#endif
}