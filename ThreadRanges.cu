#include "ThreadRanges.h"

#include <thrust/for_each.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/logical.h>
#include <thrust/sort.h>

#include <cassert>


#include "AffectedChannels.h"


using thrust::make_counting_iterator;
using thrust::raw_pointer_cast;
using thrust::device_pointer_cast;


class RangeDetector{
	AffectedChannels m_affectedChannels;
	int m_ranges;
	const int *m_rangeBounds;

public:
	RangeDetector(AffectedChannels affectedChannels, const int *rangeBounds, int ranges):
	  m_affectedChannels(affectedChannels), m_rangeBounds(rangeBounds), m_ranges(ranges){}
	__device__ __forceinline__
	void operator()(int chInd)const{
		//printf("%d\n", chInd);
		const int *chList=m_affectedChannels.AffectedRow(chInd);
		const int listLen=m_affectedChannels.RowLen(chInd);
		int *threadRanges=m_affectedChannels.m_threadRanges+33*chInd;

		for(int i=0; i<33; ++i){
			threadRanges[i]=listLen;
		}

		threadRanges[0]=0;
		//int prevRangeInd=0;
		int range=0;
		int accum=0;
		for(int affRnInd=0; affRnInd<listLen; ++affRnInd){
			int affRn=chList[affRnInd];

			while(range<m_ranges&&affRn>=m_rangeBounds[range]){
				threadRanges[range+1]=threadRanges[range]+accum;
				++range;
				accum=0;
			}
			++accum;

		}
	}
};


class ChsChecker{
	int m_chCount;
public:
	ChsChecker(int chCount):m_chCount(chCount){}

	__device__ inline
	int operator()(int chInd)const{
		//if(chInd<0||chInd>=m_chCount)
		//	printf("%d\n", chInd);
		return chInd>=0&&chInd<m_chCount;
	}
};

//could't make it with one functor due to a stupid CUDA problem
struct RangesValidator1{
	int m_maxListLen;

	explicit RangesValidator1(int maxListLen):m_maxListLen(maxListLen){}

	__device__ inline
	bool operator()(int ind)const{
		return ind>=0&&ind<=m_maxListLen;
	}
};

//could't make it with one functor due to a stupid CUDA problem
struct RangesValidator2{
	AffectedChannels m_affectedChannels;
	int m_ranges;
	const int *m_rangeBounds;

	explicit RangesValidator2(AffectedChannels affectedChannels, const int *rangeBounds, int ranges):
	  m_affectedChannels(affectedChannels), m_rangeBounds(rangeBounds), m_ranges(ranges){}

	__device__ inline
	bool operator()(int chInd)const{
		const int *chList=m_affectedChannels.AffectedRow(chInd);
		const int listLen=m_affectedChannels.RowLen(chInd);
		int *threadRanges=m_affectedChannels.m_threadRanges+33*chInd;
		assert(threadRanges[0]==0);
		for(int r=0; r<m_ranges; ++r){
			for(int i=threadRanges[r]; i<threadRanges[r+1]; ++i){
				int affRn=chList[i];
				if(r!=0&&affRn<m_rangeBounds[r-1]){
					printf("Problem (1) with reaction: %d, range: %d\n", chInd, r);
					return false;
				}
				if(affRn>=m_rangeBounds[r]){
					printf("Problem (2) with reaction: %d, range: %d  %d %d %d\n", chInd, r, i, affRn, m_rangeBounds[r]);
					return false;
				}
			}
		}
		return true;
	}
};


struct BackIndexer
{
	int const *m_backIndex;
	int const m_chsCount;

	BackIndexer(int const *backIndex, int chsCount):m_backIndex(backIndex), m_chsCount(chsCount){}
	__device__ inline
	int operator()(int ind)const{
		//if(ind<0||ind>=m_chsCount)
		//	printf("%d\n", ind);
		//my_assert(ind>=0&&ind<m_chsCount);
		int res=m_backIndex[ind];
		//if(res<0||res>=m_chsCount)
		//	printf("%d\n", res);
		//my_assert(res>=0&&res<m_chsCount);
		return res;
	}
};

void DetectThreadRanges(AffectedChannels affectedChannels, int channelsCount, thrust::device_vector<int> &elemsInRange, int rangesInArray, int maxListLen)
{
	
	thrust::for_each(make_counting_iterator<int>(0), make_counting_iterator<int>(0)+channelsCount, 
		RangeDetector(affectedChannels, raw_pointer_cast(&elemsInRange[0]), rangesInArray));
	
	assert(thrust::all_of(device_pointer_cast(affectedChannels.m_threadRanges), device_pointer_cast(affectedChannels.m_threadRanges)+33*channelsCount, 
		RangesValidator1(maxListLen)));

	assert(thrust::all_of(make_counting_iterator<int>(0), make_counting_iterator<int>(0)+channelsCount, 
		RangesValidator2(affectedChannels, raw_pointer_cast(&elemsInRange[0]), rangesInArray)));
}