//needs all 32 threads in a warp to run this function
//val is an input value for the current thread
//takes extra array of 64 elements in a shared memory to perform summation
template <typename T>
__forceinline__
__device__ T inclusive_warp_scan(int idx, T val, volatile T *s_data)
{
	my_assert(idx<warpSize);
	my_assert(s_data[idx]==(T)0);
	
//	s_data[idx]=0.;
	idx+=warpSize;
	my_assert(idx<warpSize*2);
	T t=s_data[idx]=val;
	
#ifndef NDEBUG
	if(idx==32){
		for(int i=0; i<warpSize; ++i){
			if(s_data[i]!=0.)
				printf("error in inclusive_warp_scan: must be zeros %d, %e\n", i, s_data[i]);
		}
	}
#endif

	s_data[idx]=t=t+s_data[idx-1];
	s_data[idx]=t=t+s_data[idx-2];
	s_data[idx]=t=t+s_data[idx-4];
	s_data[idx]=t=t+s_data[idx-8];
	s_data[idx]=t=t+s_data[idx-16];
	return s_data[idx];
}

//reduction-based function that detects if a requested partial sum ("val2Search") is contained within
//a chunk of array "arr" that has "warpsInChunk"*warpSize length
//"add" value stores partial sum from previous chunks
//total accumulated value is stored in the last element of the auxilary array, "s_aux_for_scan[63]"
__device__ __forceinline__
bool IsFoundInChunk(int inWarpIdx, PropReal_t const *arr, PropReal_t val2Search, int warpsInChunk, volatile PropReal_t *s_aux_for_scan, PropReal_t &add)
{
	add=s_aux_for_scan[63];
	volatile PropReal_t threadSum=0;

	//every thread in a warp accumulates values with a warpSize stride
	for(int i=0; i<warpsInChunk; ++i){
		const int ind=warpSize*i+inWarpIdx;
		volatile PropReal_t val1=arr[ind];
		threadSum+=val1;
	}

	//partial sum for a thread, also is accumulated in s_aux_for_scan[32+inWarpIdx]
	PropReal_t scan=inclusive_warp_scan(inWarpIdx, threadSum, s_aux_for_scan);
	
	if(inWarpIdx==0)
		s_aux_for_scan[63]+=add;

	return s_aux_for_scan[63]>=val2Search;
}