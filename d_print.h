#pragma once

#include <ostream>
#include <thrust/host_vector.h>


template <typename T>
struct Identity
{
	Identity(){}
	T operator()(T arg)const
	{
		return arg;
	}
};

//not for direct use
template <typename TTo, typename T=TTo, typename Tr=Identity<TTo> >
class DevArrayPrinter
{
	//typedef TTo TTo_t;
	template <typename TTof, typename Tf, typename Trf>
	friend
	std::ostream &operator<<(std::ostream &str, DevArrayPrinter<TTof, Tf, Trf> const arg);
	
	template <typename TTof, typename Tf, typename Trf>
	friend
	std::ostream &operator<<(std::ostream &str, DevArrayPrinter<const TTof, Tf, Trf> const arg);

	template <typename Tf>
	friend
	DevArrayPrinter<Tf> DevArrStreamer(thrust::device_ptr<Tf> ptr, int sz);

	template <typename TTof, typename Tf>
	friend
	DevArrayPrinter<TTof, Tf> DevArrStreamer(thrust::device_ptr<Tf> ptr, int sz);

	template <typename TTof, typename Tf, typename Trf>
	friend
	DevArrayPrinter<TTof, Tf, Trf> DevArrStreamer(thrust::device_ptr<Tf> ptr, int sz, Trf tr);
	
	
	thrust::device_ptr<T> m_ptr;
	int m_sz;
	Tr m_tr;
private:
	DevArrayPrinter(thrust::device_ptr<T> ptr, int sz, Tr tr=Identity<TTo>()):
	  m_ptr(ptr), m_sz(sz), m_tr(tr){}
};

template <typename T>
DevArrayPrinter<T> DevArrStreamer(thrust::device_ptr<T> ptr, int sz)
{
	return DevArrayPrinter<T>(ptr, sz, Identity<T>());
};


template <typename TTo, typename T>
DevArrayPrinter<TTo, T> DevArrStreamer(thrust::device_ptr<T> ptr, int sz)
{
	return DevArrayPrinter<TTo, T, Identity<TTo> >(ptr, sz, Identity<TTo>());
};

template <typename T>
DevArrayPrinter<T, T, Identity<T> > DevArrStreamer(thrust::device_vector<T>  &v)
{
	return DevArrStreamer<T, T>(v.data(), v.size());
};

template <typename TTo, typename T, typename Tr>
DevArrayPrinter<TTo, T, Tr> DevArrStreamer(thrust::device_ptr<T> ptr, int sz, Tr tr)
{
	return DevArrayPrinter<TTo, T, Tr>(ptr, sz, tr);
};

template <typename TTo, typename T, typename Tr>
std::ostream &operator<<(std::ostream &str, DevArrayPrinter<TTo, T, Tr> const arg)
{
	thrust::host_vector<TTo> dummy(arg.m_sz);
	thrust::copy(arg.m_ptr, arg.m_ptr+arg.m_sz, dummy.begin());
	
	int lastInd=dummy.size()-1;
	for(int j=0; j<lastInd; ++j)
	{ 
		str<<arg.m_tr(dummy[j])<<" ";
	}

	if(lastInd>=0)
		str<<arg.m_tr(dummy[lastInd]);

	return str;
}

template <typename TTo, typename T, typename Tr>
std::ostream &operator<<(std::ostream &str, DevArrayPrinter<const TTo, T, Tr> const arg)
{
	thrust::host_vector<TTo> dummy(arg.m_sz);
	thrust::copy(arg.m_ptr, arg.m_ptr+arg.m_sz, dummy.begin());
	
	int lastInd=dummy.size()-1;
	for(int j=0; j<lastInd; ++j)
	{ 
		str<<arg.m_tr(dummy[j])<<" ";
	}

	if(lastInd>=0)
		str<<arg.m_tr(dummy[lastInd]);

	return str;
}
