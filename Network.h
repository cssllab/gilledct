#pragma once

#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable: 4100 4512 4127)
#endif

#include <thrust/device_vector.h>

#ifdef _MSC_VER
#pragma warning (pop)
#endif


struct Network
{
	int *d_ChannelBegins;
	int *d_ChannelReactants;

	float *d_ChannelConstants;
	char *d_ChannelTypes;

	Network():d_ChannelBegins(NULL), d_ChannelReactants(NULL),
		d_ChannelConstants(NULL), d_ChannelTypes(NULL)
#ifdef ODM
#ifdef SORTED_INDICES_LIST
		,d_reactions_sorted(NULL), d_index_sorted(NULL)
#endif
#endif
	{}
};

