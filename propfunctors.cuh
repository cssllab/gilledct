#pragma once

#include <thrust/functional.h>

//determine reaqction's order
//returns -1 in case of error
class ChannelOrderDetector
{
public:
	ChannelOrderDetector(){}

	__device__ inline
	char operator()(int rn)const
	{

		int startInd=REACTION_ROW_BEGIN(rn);
		int endInd=REACTION_ROW_BEGIN(rn+1);
		char firstOrder=0, secondOrder=0;
				
		for(int i=startInd; i<endInd; ++i)
		{
			int value=GET_REACTANT(i);
			char rate; int rInd;
			split(value, rate, rInd);
			if(rate>=0)
				continue;

			if(firstOrder==0)
			{
				firstOrder=rate;
			}
			else
			{
				secondOrder=rate;
			}
		}
		if(firstOrder==-2)
			return 3;
		if(firstOrder==-1&&secondOrder==-1)
			return 2;
		if(firstOrder==-1)
			return 1;
		assert(false&&"algo error");
		return -1;
	}
};

struct Splitter
{
	int operator ()(int arg)const
	{
		int r; char dummy;
		split(arg, dummy, r);
		return r;
	}
};


class ChByRatesSorter
{
public:
	bool operator()(int combinedIndLhs, int combinedIndRhs)
	{
		char rOrderLhs, rOrderRhs;
		split(combinedIndLhs, rOrderLhs, combinedIndLhs);
		split(combinedIndRhs, rOrderRhs, combinedIndRhs);
		return (rOrderLhs<rOrderRhs);
	}
};


class ReactantsFirer
{
	int *m_rowPop;//population

	inline __device__
	void Fire(int ch2Fire)
	{
		int const beginInd=REACTION_ROW_BEGIN(ch2Fire);
		int const endInd=REACTION_ROW_BEGIN(ch2Fire+1);

		for(int i=beginInd; i<endInd; ++i)
		{
			int ind=GET_REACTANT(i);
			char order;
			split(ind, order, ind);
			if(m_rowPop[ind]+order<0){//it might happen rarely due to floating point roinding errors while making partial propensities sum
				m_rowPop[ind]=0;
			}else
				m_rowPop[ind]+=order;
		}

	}
public:
	inline __host__ __device__
	ReactantsFirer(int *rP):
		m_rowPop(rP){}

	inline __device__
	void operator()(int ch2Fire)
	{
		Fire(ch2Fire);
	}
};


class GEThan:public thrust::unary_function<PropReal_t, bool> 
{
	PropReal_t m_t;
public:
	explicit GEThan(PropReal_t t):m_t(t){}

	__device__ inline
	bool operator()(PropReal_t t)const
	{
		return t>=m_t;
	}
};