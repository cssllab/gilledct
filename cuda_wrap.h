#pragma once

__host__ inline
bool operator ==(cudaExtent const &lhs, cudaExtent const &rhs)
{
	if(lhs.width!=rhs.width)
		return false;
	if(lhs.height!=rhs.height)
		return false;
	if(lhs.depth!=rhs.depth)
		return false;
	return true;
}