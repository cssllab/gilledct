#pragma once
#include "d_print.h"
#include "CudaPitched2D.h"
#include "CummStat.h"
#include "defines.h"
#include "realtype.h"

#define ODM//use Optimize Direct Method if defined

#ifdef ODM

#define CHANNELS_PER_REACTIONS_LIST


#endif//#ifdef ODM


//define only one of these
#define USE_TEXTURES //use textures for read-only memory acess

struct CSCHostMatrixData;

#ifdef ODM
void AllPropensities(int chsCount);
#endif


#include "AffectedChannels.h"


void DataToConstMemory(CSCHostMatrixData const &matrix, 
					   thrust::host_vector<float> &chConstants,
					   int reactantsCount);

extern "C" void TestKernelInv(int *data, size_t sz);

void InitDeviceArrays(int chsCount,  thrust::host_vector<int> const &reactantsCount, 
					  int statesCount,  int numOfSteps, PropReal_t logStep, int elementsInChunk, bool storeStatistics);

__host__ void CreateRandomSeedsUint4(int statesCount);

__host__ void SelectReactions2Fire();

void FireReactions();

void ComputeTimes();
void Checking();


void TestFindingKernel(int realizationsCount, int chsCount, int rctsCount, int loops, int rangesInArray, bool extraCheck,
	PropSumReal_t *propTotalSum,
	int *reactionToFirePos,
	int predictedIndex, PropReal_t *randomNumbers, 
	int const *rangesIndices);


void RunBothKernels(int realizationsCount, int chsCount, int rctsCount, int loops, int rangesInArray, bool extraCheck,
	PropSumReal_t *propTotalSum,
	int *reactionToFirePos,
	int predictedIndex, PropReal_t *randomNumbers, 
	int const *rangesIndices,
	int iteration, float eps,
	bool storeStatistics
	);

void BindPropensities();

void TestAdvancingKernel(int realizationsCount, int chsCount, int rctsCount, int loops, bool extraCheck,
	PropSumReal_t *propTotalSum,
	int *reactionToFirePos,
	int predictedIndex, PropReal_t *&randomNumbersCurr, PropReal_t *&randomNumbersNext,
	int const *rangesInd,
	int rangesInArray,
	int iteration, float eps,
	bool storeStatistics
	);


void InitRandomNumbersSet(int realizationsCount, PropReal_t *randomNumbers);


void InitSortedIndicesAndPropSum(int realizationsCount, int channelsCount, int rangesInArray, 
	int *&reactionToFire, PropSumReal_t *&propSums,
	PropReal_t *&randomNumbersCurr, PropReal_t *&randomNumbersNext);

void SortIndicesByPropDescAndPropRecompute(int realizationsCount, int channelsCount, int rangesInArray, 
	thrust::device_vector<int> &elemsInRange,
	PropSumReal_t *propTotalSum,
	int iterationMade, PropReal_t minTime, 
	int maxListLen);

PropReal_t MinTime();

int StatesReachedTime(PropReal_t time);

void LogStatistics(int logStepsMade, int it, float time, int realizationsCount, PropSumReal_t const *totalPropSum);

void cummulativeStatistics(float totalTime, std::string statPrefix);


void LogPopulation();
//void LogPropensitiesPrefixSum();
//void LogReactions2Fire();
#ifdef ODM
void LogPropenseties();
#endif
void LogTimes();

void LogReactantIndices(int rctsCount);
void LogChannelBegins(int chsCount);
void LogSortedReactions(int chsCount);
void LogIndexing(int rctsCount);
void LogChannelConstants(int chsCount);
void LogChannelOrders(int chsCount);

//for debugging purposes
void ShowAllPopulations(std::ostream &os);

