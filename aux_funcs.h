#pragma once
struct CSCHostMatrixData;
struct AffectedChannels;

void MakeAssocChannelsList(CSCHostMatrixData const &matrix, int channelsCount, int reactantsCount, AffectedChannels &affectedChannels, int &longestList);