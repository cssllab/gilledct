#pragma once
#include <vector>
#include "realtype.h"

struct CummStat
{
	PropReal_t min;
	PropReal_t max;
	PropReal_t mean;
	PropReal_t stddev;
};

void print(std::vector<std::vector<CummStat>> const &cummStat, const char *base_fn, float totalTime);