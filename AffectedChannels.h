#pragma once
#include "defines.h"
struct AffectedChannels
{
	int *m_rowStarts;
	int *m_affcetedLists;
	int *m_threadRanges;
	
	int m_totalLen;

	__device__ __host__ inline
	int const *AffectedRow(int ind)const{
		return m_affcetedLists+m_rowStarts[ind];
	}

	__device__ inline
	int RowLen(int ind)const{
		return m_rowStarts[ind+1]-m_rowStarts[ind];
	}

	__host__ inline
	void Init(int channels, int totalSize){
		CHECK_CUDA_ERRORS();
		m_totalLen=totalSize;
		cudaMalloc((void **)&m_rowStarts, (channels+1)*sizeof(*m_rowStarts));
		cudaMalloc((void **)&m_affcetedLists, totalSize*sizeof(*m_affcetedLists));
		cudaMalloc((void **)&m_threadRanges, 33*channels*sizeof(*m_threadRanges));
		CHECK_CUDA_ERRORS();
	}

	__host__ inline
	void Free(){
		CHECK_CUDA_ERRORS();
		cudaFree(m_rowStarts);
		cudaFree(m_affcetedLists);
		cudaFree(m_threadRanges);
		CHECK_CUDA_ERRORS();
	}
};