#pragma once
#include <cassert>
#include <stdexcept>

template <typename T> class CudaPitched3D;

template <typename T>
class CudaPitched2D
{
	/*template <typename T1, typename T2>
	friend
	void CopyASliceFrom2DTo3D(CudaPitched2D<T1> c2d, CudaPitched3D<T2> c3d, int sliceN);*/

	T *m_mem;
	std::size_t m_pitch;
	std::size_t m_width, m_height;

public:
	__host__
	CudaPitched2D():m_mem(NULL), m_pitch(0), m_width(0), m_height(0)
	{}

	__host__
	void Init(std::size_t width, std::size_t height)
	{

		if(cudaMallocPitch((void **) &m_mem, &m_pitch, width*sizeof(T), height)!=cudaSuccess)
			throw std::runtime_error("can not allocate a pitched array");
		m_width=width;
		m_height=height;
	}

	__host__
	void ZeroMatrix()
	{
		if(cudaMemset2D (m_mem, m_pitch, 0,  m_width*sizeof(T),  m_height)!=cudaSuccess)
			throw std::runtime_error("Can't set zeros in CudaPitched2D");
	}

	__host__
	void Destroy()
	{
		if(m_mem)
			cudaFree(m_mem);
	}

	__host__ __device__
	__forceinline__
	T *Row(int r)
	{
		//my_assert(r<(int)m_height);
		return (T *)((char*)m_mem + r * m_pitch);
	}

	__host__ __device__ __forceinline__
	T const *Row(int r)const
	{
		return (T *)((char*)m_mem + r * m_pitch);
	}

	__host__ __device__ __forceinline__
	int RowStartInd(int r)const{

		return (r*m_pitch)/sizeof(T);
	}

	__host__ __device__
	std::size_t Height()const{return m_height;}

	__host__ __device__
	std::size_t Width()const{return m_width;}//width in elements, not in bytes!

	__device__
	void CopyRowToByMP(int idx, CudaPitched2D &other, int r)const
	{
		my_assert(other.Height()==this->Height());
		my_assert(other.Width()>=Width());

		T *dst=other.Row(r);
		T const *src=Row(r);
		CopyArrayByMP(idx, dst, src, other.Width());
	}

	__host__
	void CopyTo(CudaPitched2D &other)const
	{
		assert(other.Height()==this->Height());
		assert(other.Width()>=Width());
		cudaMemcpy2D(other.m_mem, other.m_pitch, 
			m_mem, m_pitch, 
			Width()*sizeof(T), Height(), cudaMemcpyDeviceToDevice);
	}
};