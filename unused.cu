__device__ inline
void AdvanceTimeStep(uint4 *locTimeSeed, float *timeStep, PropReal_t totalPropSum, float *timeArray)
{
	float rn=HybridTausRng(locTimeSeed);
		
	//*timeStep=log(1.f/rn)/totalPropSum;
	*timeStep=log(1.f/rn)/totalPropSum;
	//printf("system: %d, step selected: %e channel selected: %d, prpsSum: %f, rn: %f\n", stateIdx, step[warpInBlockId], *r2Fire, prpsPrefixSum[chsCount], rn);
	my_assert(timeStep>=0);
		
	*timeArray+=*timeStep;
}


__global__ 
__launch_bounds__(256)
void FindIndexOfReactionToBeFiredKernel(int realizationsCount, int chsCount,
							   CudaPitched2D<float> propensities, //chsCount x realizationsCount
							   int *reactionsToFire,
							   int predictedIndex,
							   float const *val2Find,
							   float const*addition,
							   int const *rangesIndices, int rangesInArray,
							   float *totalPropSum,
							   int const *foundInRange)
{

	int const idx=blockDim.x * blockIdx.x + threadIdx.x;
	int const realizationIdx=idx>>5;

	if(realizationIdx>=realizationsCount)
		return;

	int const inWarpIdx=idx&31;

	int range=foundInRange[realizationIdx];
	my_assert(range<rangesInArray);
	my_assert(val2Find[realizationIdx]<=totalPropSum[realizationIdx*rangesInArray+range]);
	if(range>0){
		my_assert(val2Find[realizationIdx]>=totalPropSum[realizationIdx*rangesInArray+range-1]);
	}

	//float add;
	int shift;
	if(range==0){
//		add=0.f;
		shift=0;
	}
	else{
//		add=totalPropSum[realizationIdx*rangesInArray+range-1];//total partial sum for previous chunk
		shift=rangesIndices[range-1];
	}

	__shared__ float s_aux_for_scan[64*WARPS_IN_BLOCK];
	
	int const warpInBlockIdx=realizationIdx%WARPS_IN_BLOCK;

	my_assert(addition[realizationIdx]<val2Find[realizationIdx]);

	int const warpsInChunkCount=DIV_UP(predictedIndex, warpSize);
	int const elementsInChunk=warpSize*warpsInChunkCount;
//	int const predictedChunksCount=DIV_UP(chsCount, elementsInChunk);

	//unpuck information about position
	int const threadContains=reactionsToFire[realizationIdx]&31;
	int const chunkInd=reactionsToFire[realizationIdx]>>5;

	my_assert(threadContains>=0&&threadContains<32);
//	my_assert(chunkInd>=0&&chunkInd<warpsInChunkCount);

	float prop;
	if(inWarpIdx<warpsInChunkCount)
		prop=(propensities.Row(realizationIdx)+chunkInd*elementsInChunk+shift)[threadContains+inWarpIdx*warpSize];
	else
		prop=0;

//	if(inWarpIdx<warpsInChunkCount)
	//	printf("inWarpIdx=%d, prop=%f\n", inWarpIdx, prop);

	(s_aux_for_scan+64*warpInBlockIdx)[inWarpIdx]=0.;

	if(inWarpIdx==31)
		(s_aux_for_scan+64*warpInBlockIdx)[63]=0;

	float scan=inclusive_warp_scan(inWarpIdx, prop, s_aux_for_scan+64*warpInBlockIdx);
	if(inWarpIdx==31){
		my_assert((s_aux_for_scan+64*warpInBlockIdx)[63]+addition[realizationIdx]>=val2Find[realizationIdx]);
	//	if(range>0)
			printf("looking for %e, addition is: %e, upper bound is %e, threadContains=%d, chunkId=%d\n", 
				val2Find[realizationIdx], addition[realizationIdx], (s_aux_for_scan+64*warpInBlockIdx)[63]+addition[realizationIdx], threadContains, chunkInd);
	}

	/*if(inWarpIdx==31){
		if(scan+addition[realizationIdx]+add< val2Find[realizationIdx])
			printf("Second kernel: value: %e chunk: %d thread: %d addition: %e upper bound: %e. Sum: %e \n", 
				val2Find[realizationIdx], chunkInd, threadContains, addition[realizationIdx], scan+addition[realizationIdx], scan);
	}*/
	

	int warpInd=FirstThreadIdThatGE(scan+addition[realizationIdx], val2Find[realizationIdx]);
	if(inWarpIdx==31/*&&range>0*/)
		printf("found in %d warp; result index is: %d\n", warpInd, chunkInd*elementsInChunk+shift+threadContains+warpInd*warpSize);

	/*if(inWarpIdx==0)
		printf("realzId=%d, was looking for: %e, found in pos: %d, range %d, chunk %d\n", 
		realizationIdx,  val2Find[realizationIdx], chunkInd*elementsInChunk+shift+threadContains+ind*warpSize, range, chunkInd);*/

	//my_assert(ind>=0&&ind<chsCount);
	
	reactionsToFire[realizationIdx]=chunkInd*elementsInChunk+shift+threadContains+warpInd*warpSize;
}


__global__ 
__launch_bounds__(256)
void FindIndexOfReactionToBeFiredKernel(int realizationsCount, int chsCount, int rangesInArray,
							   uint4 *rSeeds,
							   CudaPitched2D<float> propopensities, //chsCount x realizationsCount
							   float *totalPropSum,
							   int *reactionsToFire,
							   int predictedIndex,
							   float *val2Find,
							   float *addition, 
							   int const *rangesIndices,
							   int *foundInRange)
{

	int const idx=blockDim.x * blockIdx.x + threadIdx.x;
	int const realizationIdx=idx>>5;

	if(realizationIdx>=realizationsCount)
		return;

	__shared__ float s_val2Search[WARPS_IN_BLOCK];

	int const inWarpIdx=idx&31;
	
	int const warpInBlockIdx=realizationIdx%WARPS_IN_BLOCK;

	if(inWarpIdx==0){
		float rn=HybridTausRng(&rSeeds[realizationIdx]);
		s_val2Search[warpInBlockIdx]=rn*totalPropSum[realizationIdx*rangesInArray+rangesInArray-1];
		//s_val2Search[warpInBlockIdx]=374789.5f;
		val2Find[realizationIdx]=s_val2Search[warpInBlockIdx];
	//	printf("realzId=%d, looking for %e. %e %e\n", realizationIdx, val2Find[realizationIdx], rn, totalPropSum[realizationIdx*rangesInArray+rangesInArray-1]);
	}

			

	float rangeBound;
	if(inWarpIdx<rangesInArray)
		rangeBound=totalPropSum[realizationIdx*rangesInArray+inWarpIdx];
	else
		rangeBound=1E+30;

	int range=FirstThreadIdThatGE(rangeBound, s_val2Search[warpInBlockIdx])-1;

	//my_assert(range>=0&&range<rangesInArray);
		
	int warpsInChunkCount=DIV_UP(predictedIndex, warpSize);

	//int const chunks=WarpChunks(arrLen);

	float add;
	int shift;
	if(range==-1){
		add=0.f;
		shift=0;
	}
	else{
		add=totalPropSum[realizationIdx*rangesInArray+range];//total partial sum for previous chunk
		shift=rangesIndices[range];
	}

		
	__shared__ PropReal_t s_aux_for_scan[64*WARPS_IN_BLOCK];//auxilary shared array for finding per-warp prefix sum
	
	(s_aux_for_scan+64*warpInBlockIdx)[inWarpIdx]=0.;

	int foundPos=FindInArray(inWarpIdx, warpInBlockIdx, s_val2Search[warpInBlockIdx], add, propopensities.Row(realizationIdx)+shift, chsCount-shift, s_aux_for_scan+warpInBlockIdx*64);
	if(inWarpIdx==0)
			reactionsToFire[realizationIdx]=foundPos;
}



__global__ 
//__launch_bounds__(256)
void FindingReactionToFireKernel(int realizationsCount, int chsCount, int rctsCount,
							   uint4 *rSeeds,
							   CudaPitched2D<float> propopensities, //chsCount x realizationsCount
							   float *totalPropSum,
							   int loops,
							   const int *sortedIndices,
							   int *reactionsToFire)
{
	int volatile const idx=blockDim.x * blockIdx.x + threadIdx.x;
	int volatile const realizationIdx=idx>>5;
	int volatile const inWarpIdx=idx&31;

	int inBlockWarpIdx=realizationIdx%WARPS_IN_BLOCK;



//	__shared__ float add;

//	if(inWarpIdx==31)
	//	add=0.f;

	typedef float accType;

	if(realizationIdx>=realizationsCount)
		return;

	uint4 localRSeed;//seeds for finding a reaction to be fired
	accType chPropSum=totalPropSum[realizationIdx];//total propensities sum
	__shared__ accType s_val2Search[WARPS_IN_BLOCK];//reaction with this partial sum of propensities will be looking for firing
	__shared__ accType s_aux_for_scan[64*WARPS_IN_BLOCK];//auxilary shared array for finding per-warp prefix sum
	
	localRSeed=rSeeds[realizationIdx];
	

#pragma unroll 4
	for(int l=0; l<loops; ++l){

		if(inWarpIdx==0){
			s_val2Search[inBlockWarpIdx]=HybridTausRng(&localRSeed)*chPropSum;
		}

		int foundPos=FindInArray(inWarpIdx, inBlockWarpIdx, s_val2Search[inBlockWarpIdx], propopensities.Row(realizationIdx), chsCount, s_aux_for_scan+inBlockWarpIdx*64);

		//int foundPos=s_val2Search[inBlockWarpIdx]/chPropSum[inBlockWarpIdx]*chsCount;
		
		if(foundPos<0)
			continue;

		if(inWarpIdx==0)
			reactionsToFire[realizationIdx]=sortedIndices[foundPos];
	}//loop

	

	if(inWarpIdx==0){
		//totalPropSum[realizationIdx]=chPropSum;
		rSeeds[realizationIdx]=localRSeed;
	}
}



template <bool storeValue2Find>
__global__ __launch_bounds__(256)
void FindRangeAndChunkOfReactionToBeFiredKernel(int realizationsCount, int chsCount, int rangesInArray,
							   uint4 *rSeeds,
							   CudaPitched2D<float> propopensities, //chsCount x realizationsCount
							   float const *totalPropSum,
							   int *reactionsToFire,
							   int predictedIndex,
							   float *val2Find,
							   int const *rangesIndices
							 //  , float *addition, 
							 //  , int *foundInRange
							 )
{

	int const idx=blockDim.x * blockIdx.x + threadIdx.x;
	int const realizationIdx=idx>>5;

	if(realizationIdx>=realizationsCount)
		return;

	__shared__ float s_val2Search[WARPS_IN_BLOCK];

	int const inWarpIdx=idx&31;
	
	int const warpInBlockIdx=realizationIdx%WARPS_IN_BLOCK;

	if(inWarpIdx==0){
		float rn=HybridTausRng(&rSeeds[realizationIdx]);
		s_val2Search[warpInBlockIdx]=rn*totalPropSum[realizationIdx*rangesInArray+rangesInArray-1];
		//s_val2Search[warpInBlockIdx]=374789.5f;
		if(storeValue2Find)
			val2Find[realizationIdx]=s_val2Search[warpInBlockIdx];
	//	printf("realzId=%d, looking for %e. %e %e\n", realizationIdx, val2Find[realizationIdx], rn, totalPropSum[realizationIdx*rangesInArray+rangesInArray-1]);
	}

			

	float rangeBound;
	if(inWarpIdx<rangesInArray)
		rangeBound=totalPropSum[realizationIdx*rangesInArray+inWarpIdx];
	else
		rangeBound=1E+30;

	int range=FirstThreadIdThatGE(rangeBound, s_val2Search[warpInBlockIdx])-1;

	//my_assert(range>=0&&range<rangesInArray);
		
	int warpsInChunkCount=DIV_UP(predictedIndex, warpSize);

	//int const chunks=WarpChunks(arrLen);

		
	__shared__ Accum_t s_aux_for_scan[64*WARPS_IN_BLOCK];//auxilary shared array for finding per-warp prefix sum
	
	(s_aux_for_scan+64*warpInBlockIdx)[inWarpIdx]=0.;

	int elemInChunk=warpsInChunkCount*warpSize;

	
	//int threadContains;
	

	float add;
	int shift;
	if(range==-1){
		add=0.f;
		shift=0;
	}
	else{
		add=totalPropSum[realizationIdx*rangesInArray+range];//total partial sum for previous chunk
		shift=rangesIndices[range];
	}

	if(inWarpIdx==31)
		(s_aux_for_scan+64*warpInBlockIdx)[63]=add;//use the last element for storing total partial sum for previous chunk

	int maxIt=DIV_UP(chsCount, warpsInChunkCount*warpSize);
	int i;

	my_assert(add<=s_val2Search[warpInBlockIdx]);
	for(i=0; i<maxIt; ++i){
		if(i==maxIt-1)
			warpsInChunkCount=(chsCount-i*warpsInChunkCount*warpSize)/warpSize;
	
		/*threadContains=FindInChunk(inWarpIdx, propopensities.Row(realizationIdx)+shift+elemInChunk*i, s_val2Search[warpInBlockIdx], warpsInChunkCount, s_aux_for_scan+64*warpInBlockIdx, add);
		if(threadContains!=-1)
			break;*/

		if(IsFoundInChunk(inWarpIdx, propopensities.Row(realizationIdx)+shift+elemInChunk*i, s_val2Search[warpInBlockIdx],  warpsInChunkCount, s_aux_for_scan+64*warpInBlockIdx, add))
			break;
	}

	my_assert(i<maxIt);

	int foundPos=FindInArray(inWarpIdx, warpInBlockIdx, s_val2Search[warpInBlockIdx], add, propopensities.Row(realizationIdx)+shift+elemInChunk*i, chsCount-(shift+elemInChunk*i), s_aux_for_scan+warpInBlockIdx*64);

	/*if(foundPos==-1)
		printf("Ooops\n");*/
		

	//if(inWarpIdx==0)
		//printf("realzId=%d, found in %d range, %d chunk\n", realizationIdx, range+1, i);

/*	my_assert((s_aux_for_scan+64*warpInBlockIdx)[63]>=s_val2Search[warpInBlockIdx]);
	my_assert(threadContains>=0);
	my_assert(threadContains<warpSize);*/

	/*__shared__ float auxSum;

	if(inWarpIdx==threadContains){
		for(int j=0; j<warpsInChunkCount; ++j)
			auxSum+=(propopensities.Row(realizationIdx)+elemInChunk*i)[j*warpSize+inWarpIdx];
	}*/

	if(inWarpIdx==0){
		/*if(threadContains==0)
			addition[realizationIdx]=add;//TODO: might be optimized, no condition really need
		else*/
		//	addition[realizationIdx]=add+(s_aux_for_scan+64*warpInBlockIdx)[31+threadContains];//adding the partial sum for the previous thread

		/*printf("First kernel: value %e detected in chunk %d with thread %d. Bounds are %e %e. Plain sum: %e, upper bound with sum: %e\n", 
			s_val2Search[warpInBlockIdx], i, threadContains, addition[realizationIdx], (s_aux_for_scan+64*warpInBlockIdx)[63], auxSum, addition[realizationIdx]+auxSum);*/

	//	my_assert(addition[realizationIdx]<s_val2Search[warpInBlockIdx]);
		//my_assert((s_aux_for_scan+64*warpInBlockIdx)[63]>=s_val2Search[warpInBlockIdx]);
		reactionsToFire[realizationIdx]=foundPos+shift+elemInChunk*i;//threadContains+warpSize*i;
	//	foundInRange[realizationIdx]=range+1;
	}
}


__global__ 
//__launch_bounds__(256)
void FindingReactionToFireKernel1(int realizationsCount, int chsCount, int rctsCount,
							   uint4 *rSeeds,
							   CudaPitched2D<float> propopensities, //chsCount x realizationsCount
							   float *totalPropSum,
							   int loops,
							   const int *sortedIndices,
							   int *reactionsToFire,
							   int predictedIndex)
{

	int const idx=blockDim.x * blockIdx.x + threadIdx.x;
	int const realizationIdx=idx>>5;

	if(realizationIdx>=realizationsCount)
		return;

	int const inWarpIdx=idx&31;
	
	int warpInBlockIdx=realizationIdx%WARPS_IN_BLOCK;
	
	float const *prop=propopensities.Row(realizationIdx);

	int const predictedChunksCount=DIV_UP(predictedIndex, warpSize);
	//int const chunks=WarpChunks(arrLen);

		
	__shared__ Accum_t s_aux_for_scan[64*WARPS_IN_BLOCK];//auxilary shared array for finding per-warp prefix sum
	__shared__ Accum_t s_val2Search[WARPS_IN_BLOCK];//reaction with this partial sum of propensities will be looking for firing
	__shared__ uint4 localRSeed[WARPS_IN_BLOCK];
	__shared__ float chPropSum[WARPS_IN_BLOCK];
	__shared__ Accum_t threadsPrefixScan[32*WARPS_IN_BLOCK];
	
	(s_aux_for_scan+64*warpInBlockIdx)[inWarpIdx]=0.;

	if(inWarpIdx==0){
		localRSeed[warpInBlockIdx]=rSeeds[realizationIdx];
		chPropSum[warpInBlockIdx]=totalPropSum[realizationIdx];
		s_val2Search[warpInBlockIdx]=HybridTausRng(&localRSeed[warpInBlockIdx])*chPropSum[warpInBlockIdx];
	}

	
	for(int l=0; l<loops; ++l){ 
		if(inWarpIdx==31)
			(s_aux_for_scan+64*warpInBlockIdx)[63]=0;
		int foundPos;
		int maxIt=DIV_UP(chsCount, predictedChunksCount*warpSize);
		for(int i=0; i<maxIt; ++i){
			foundPos=FindInPredictedChunk(inWarpIdx, predictedChunksCount, prop+i*predictedChunksCount*32, s_val2Search[warpInBlockIdx], s_aux_for_scan+64*warpInBlockIdx, threadsPrefixScan+32*warpInBlockIdx);
			if(foundPos!=-1){
				foundPos+=i*32;
				break;
			}
		}
		reactionsToFire[realizationIdx]=foundPos;
	}


	if(warpInBlockIdx==0){
		rSeeds[realizationIdx]=localRSeed[warpInBlockIdx];
		totalPropSum[realizationIdx]=chPropSum[warpInBlockIdx];
	}
}


__global__ 
__launch_bounds__(256)
void FindThreadAndChunkOfReactionToBeFiredKernel(int realizationsCount, int chsCount, int rangesInArray,
							   uint4 *rSeeds,
							   CudaPitched2D<float> propopensities, //chsCount x realizationsCount
							   float *totalPropSum,
							   int *reactionsToFire,
							   int predictedIndex,
							   float *val2Find,
							   float *addition, 
							   int const *rangesIndices,
							   int *foundInRange)
{

	int const idx=blockDim.x * blockIdx.x + threadIdx.x;
	int const realizationIdx=idx>>5;

	if(realizationIdx>=realizationsCount)
		return;

	__shared__ float s_val2Search[WARPS_IN_BLOCK];

	int const inWarpIdx=idx&31;
	
	int const warpInBlockIdx=realizationIdx%WARPS_IN_BLOCK;

	if(inWarpIdx==0){
		float rn=HybridTausRng(&rSeeds[realizationIdx]);
		//s_val2Search[warpInBlockIdx]=rn*totalPropSum[realizationIdx*rangesInArray+rangesInArray-1];
		s_val2Search[warpInBlockIdx]=374789.5f;
		val2Find[realizationIdx]=s_val2Search[warpInBlockIdx];
	//	printf("realzId=%d, looking for %e. %e %e\n", realizationIdx, val2Find[realizationIdx], rn, totalPropSum[realizationIdx*rangesInArray+rangesInArray-1]);
	}

			

	float rangeBound;
	if(inWarpIdx<rangesInArray)
		rangeBound=totalPropSum[realizationIdx*rangesInArray+inWarpIdx];
	else
		rangeBound=1E+30;

	int range=FirstThreadIdThatGE(rangeBound, s_val2Search[warpInBlockIdx])-1;

	//my_assert(range>=0&&range<rangesInArray);
		
	int warpsInChunkCount=DIV_UP(predictedIndex, warpSize);

	//int const chunks=WarpChunks(arrLen);

		
	__shared__ Accum_t s_aux_for_scan[64*WARPS_IN_BLOCK];//auxilary shared array for finding per-warp prefix sum
	
	(s_aux_for_scan+64*warpInBlockIdx)[inWarpIdx]=0.;

	int elemInChunk=warpsInChunkCount*warpSize;

	
	int threadContains;
	int maxIt=DIV_UP(chsCount, warpsInChunkCount*warpSize);
	int i;

	float add;
	int shift;
	if(range==-1){
		add=0.f;
		shift=0;
	}
	else{
		add=totalPropSum[realizationIdx*rangesInArray+range];//total partial sum for previous chunk
		shift=rangesIndices[range];
	}

	if(inWarpIdx==31)
		(s_aux_for_scan+64*warpInBlockIdx)[63]=add;//use the last element for storing total partial sum for previous chunk

	my_assert(add<=s_val2Search[warpInBlockIdx]);
	for(i=0; i<maxIt; ++i){
		if(i==maxIt-1)
			warpsInChunkCount=(chsCount-i*warpsInChunkCount*warpSize)/warpSize;
	
		threadContains=FindInChunk(inWarpIdx, propopensities.Row(realizationIdx)+shift+elemInChunk*i, s_val2Search[warpInBlockIdx], warpsInChunkCount, s_aux_for_scan+64*warpInBlockIdx, add);
		if(threadContains!=-1)
			break;
	}

	//if(inWarpIdx==0)
		//printf("realzId=%d, found in %d range, %d chunk\n", realizationIdx, range+1, i);

	my_assert((s_aux_for_scan+64*warpInBlockIdx)[63]>=s_val2Search[warpInBlockIdx]);
	my_assert(threadContains>=0);
	my_assert(threadContains<warpSize);

	/*__shared__ float auxSum;

	if(inWarpIdx==threadContains){
		for(int j=0; j<warpsInChunkCount; ++j)
			auxSum+=(propopensities.Row(realizationIdx)+elemInChunk*i)[j*warpSize+inWarpIdx];
	}*/

	if(inWarpIdx==0){
		/*if(threadContains==0)
			addition[realizationIdx]=add;//TODO: might be optimized, no condition really need
		else*/
			addition[realizationIdx]=add+(s_aux_for_scan+64*warpInBlockIdx)[31+threadContains];//adding the partial sum for the previous thread

		/*printf("First kernel: value %e detected in chunk %d with thread %d. Bounds are %e %e. Plain sum: %e, upper bound with sum: %e\n", 
			s_val2Search[warpInBlockIdx], i, threadContains, addition[realizationIdx], (s_aux_for_scan+64*warpInBlockIdx)[63], auxSum, addition[realizationIdx]+auxSum);*/

		my_assert(addition[realizationIdx]<s_val2Search[warpInBlockIdx]);
		my_assert((s_aux_for_scan+64*warpInBlockIdx)[63]>=s_val2Search[warpInBlockIdx]);
		reactionsToFire[realizationIdx]=threadContains+warpSize*i;
		foundInRange[realizationIdx]=range+1;
	}
}

__device__ inline
bool ShallStatisticsBeStored(float *timeArray,  float statisticsNextTime){
	if(*timeArray>=statisticsNextTime){
	//	my_assert(*timeArray-*timeStep<statisticsNextTime);//TODO: return it back;
		return true;
		//timeStepRatio[warpInBlockId]=(statistics.NextTime(stateIdx)-(tArr[stateIdx]-step))/step;

		//if(!(timeStepRatio[warpInBlockId]>0&&timeStepRatio[warpInBlockId]<=1))
			//printf("%d %f %f %f %f %f %f\n", innerIdx, timeStepRatio[warpInBlockId], statistics.NextTime(stateIdx), tArr[stateIdx], statistics.NextTime(stateIdx), (tArr[stateIdx]-step), step);
	}else
		return false;
}



template <bool saveStatistics>
__global__ 
//__launch_bounds__(256)
void CombinedKernelODM3(int realizationsCount, int chsCount, int rctsCount,
							   uint4 *rSeeds,
							   uint4 *tSeeds,
							   CudaPitched2D<int> prevPopulation,
							   CudaPitched2D<int> population,
							   CudaPitched2D<float> propopensities, //chsCount x realizationsCount
							   float *totalPropSum,
							   Statistics statistics,
							   float *timeArray,
							   int loops,
							   const int *sortedIndices,
							   const int *newIndices,
							   AffectedChannels affectedChannels,
							   int *reactionsToFire)
{
	int const idx=blockDim.x * blockIdx.x + threadIdx.x;
	int const realizationIdx=idx>>5;
	int const inWarpIdx=idx&31;

	int inBlockWarpIdx=realizationIdx%WARPS_IN_BLOCK;



//	__shared__ float add;

//	if(inWarpIdx==31)
	//	add=0.f;

	typedef float accType;

	if(realizationIdx>=realizationsCount)
		return;

	__shared__ uint4 localRSeed[WARPS_IN_BLOCK];//seeds for finding a reaction to be fired
	__shared__ uint4 localTSeed[WARPS_IN_BLOCK];//seeds for finding a time step
	__shared__ float timeStep[WARPS_IN_BLOCK];
	__shared__ float *ta[WARPS_IN_BLOCK];
	__shared__ accType chPropSum[WARPS_IN_BLOCK];//total propensities sum
	__shared__ accType s_val2Search[WARPS_IN_BLOCK];//reaction with this partial sum of propensities will be looking for firing
	__shared__ accType s_aux_for_scan[64*WARPS_IN_BLOCK];//auxilary shared array for finding per-warp prefix sum
	__shared__ bool s_shouldSaveStatistics[WARPS_IN_BLOCK];//flags if we need to store statistics on the current time step

	if(inWarpIdx==0){
		chPropSum[inBlockWarpIdx]=totalPropSum[realizationIdx];
		localRSeed[inBlockWarpIdx]=rSeeds[realizationIdx];
		localTSeed[inBlockWarpIdx]=tSeeds[realizationIdx];
		ta[inBlockWarpIdx]=&timeArray[realizationIdx];
	}

	float *prop=propopensities.Row(realizationIdx);
	int *reactantPopulation=population.Row(realizationIdx);
	size_t avgPos=0;

#pragma unroll 4
	for(int l=0; l<loops; ++l){

		float nextTimeStatistics;
		if(inWarpIdx==0){
			nextTimeStatistics=statistics.NextTime(realizationIdx);
			s_val2Search[inBlockWarpIdx]=HybridTausRng(&localRSeed[inBlockWarpIdx])*chPropSum[inBlockWarpIdx];
		}

		//index of a reaction to be fired in a sorted array
		int foundPos=FindInArray(inWarpIdx, inBlockWarpIdx, s_val2Search[inBlockWarpIdx], prop, chsCount, s_aux_for_scan+inBlockWarpIdx*64);
		/*if((foundPos<0||foundPos>=chsCount)&&inWarpIdx==0){
			printf("Oops %d %d, %e %e\n", foundPos, l, s_val2Search[inBlockWarpIdx], chPropSum[inBlockWarpIdx]);
			continue;
		}*/

		if(foundPos<0)
			continue;
						
		if(inWarpIdx==0){
			AdvanceTimeStep(&localTSeed[inBlockWarpIdx], &timeStep[inBlockWarpIdx], chPropSum[inBlockWarpIdx], ta[inBlockWarpIdx]);

			if(saveStatistics)
				s_shouldSaveStatistics[inBlockWarpIdx]=ShallStatisticsBeStored(ta[inBlockWarpIdx], nextTimeStatistics);
		}

		avgPos+=foundPos;

		int rtf=sortedIndices[foundPos];
		 
		if(saveStatistics){
 			if(s_shouldSaveStatistics[inBlockWarpIdx]){
				//we need to store population on the previous time step to compute population in between two time steps
				CopyArrayByMP(inBlockWarpIdx, prevPopulation.Row(realizationIdx), reactantPopulation, rctsCount);
			}
		}

		//my_assert(rtf>=0&&rtf<chsCount);

		//firing reactions and updating propensities here
		FireReaction(inWarpIdx, rtf, affectedChannels, reactantPopulation, prop, newIndices, &chPropSum[inBlockWarpIdx], s_aux_for_scan+inBlockWarpIdx*64);

		bool shallStop=false;
		
		if(saveStatistics){
			if(s_shouldSaveStatistics[inBlockWarpIdx]){ 
				do{//may have multiple steps to write here
					float timeStepRatio=(statistics.NextTime(realizationIdx)-(*ta[inBlockWarpIdx]-timeStep[inBlockWarpIdx]))/timeStep[inBlockWarpIdx];
				//	if(innerIdx==0)
					//	printf("%d ratio is: %f for realization: %d %f %f %f\n", j++, timeStepRatio, stateIdx, tArr[stateIdx]-step[warpInBlockId], statistics.NextTime(stateIdx), tArr[stateIdx]);

					shallStop=!statistics.StorePopulationMP(inWarpIdx, realizationIdx, 
						prevPopulation.Row(realizationIdx), reactantPopulation, timeStepRatio);
					if(shallStop){
				//		if(innerIdx==0)
					//	printf("exited(?)\n");
						break;//trying to save statistics behind the limit
					}
				}while(statistics.NextTime(realizationIdx)<=*ta[inBlockWarpIdx]);
			}

			if(shallStop)//reached the end
				break;
		}
	}//(int l=0; l<loops; ++l), internal kernel's loop

	if(inWarpIdx==0){
		reactionsToFire[realizationIdx]=avgPos/(float)loops;
		totalPropSum[realizationIdx]=chPropSum[inBlockWarpIdx];
		rSeeds[realizationIdx]=localRSeed[inBlockWarpIdx];
		tSeeds[realizationIdx]=localTSeed[inBlockWarpIdx];
		timeArray[realizationIdx]=*ta[inBlockWarpIdx];
	}
}


float RunMainKernel(int realizationsCount, int chsCount, int rctsCount, int loops,
	const int *sortedIndices,
	const int *newIndices,
	float *propTotalSum,
	int *reactionToFirePos)
{
	int const BLOCK_SIZE=32*WARPS_IN_BLOCK;
	int const BLOCKS=DivUp(realizationsCount, WARPS_IN_BLOCK);

	CombinedKernelODM3<false><<<BLOCKS, BLOCK_SIZE>>>(realizationsCount, chsCount, rctsCount,// propPrefixSumLenAndPropSumLen,
			raw_pointer_cast(g_selectorSeeds.data()), raw_pointer_cast(g_tauSeeds.data()),
			d_prevReactantPopulation,
			d_reactantPopulation,
			d_propensities, propTotalSum,
			g_Statistics,
			raw_pointer_cast(g_times.data()),
			loops,  sortedIndices, newIndices, g_affectedChannels, reactionToFirePos);

	cudaError_t err=cudaGetLastError();
	if(err!=cudaSuccess){
		if(err==cudaErrorInvalidConfiguration){
			cerr<<"Wrong main kenrel configuration:\n"<<
				"\t"<<BLOCKS<<"x"<<BLOCK_SIZE<<"\n";
		}else{
			cerr<<"Kernel failed with code "<<err<<"\n";
		}
		exit(EXIT_FAILURE); 
	}
	return thrust::reduce(device_pointer_cast(reactionToFirePos), device_pointer_cast(reactionToFirePos)+realizationsCount, 0)/(float)realizationsCount;
}



extern __shared__ float shared[];
__global__ void SelectReaction2FireKernel(int chsCount, int statesCount, 
										  CudaPitched2D<float> prpsPrefixSum,
										  uint4 *seed,
										  int *rctns2Fire)
{
	PROLOG;

	my_assert(stateIdx<prpsPrefixSum.Height());
	float *row=prpsPrefixSum.Row(stateIdx);


	//printf("innerIdx=%d, stateIdx=%d\n", toFind[stateIdx]);
	float *toFind=shared;
	if(innerIdx==0)
	{
		toFind[stateIdx]=HybridTausRng(seed+stateIdx)*row[chsCount];
		
		my_assert(toFind[stateIdx]<=row[chsCount]);
		//printf("toFind[%d]=%f\n", stateIdx, toFind[stateIdx]);
	//	printf("inside: innerIdx=%d, stateIdx=%d, toFind=%f\n", innerIdx, stateIdx, toFind[stateIdx]);
	}
	
	//__syncthreads();

	
//	printf("innerIdx=%d, stateIdx=%d, toFind=%f\n", innerIdx, stateIdx, tf);

	
	FindFirstGreaterThanMP(innerIdx, chsCount, row+1, toFind[stateIdx],
		&rctns2Fire[stateIdx]);

	if(innerIdx==0)
	{
		if(rctns2Fire[stateIdx]==-1)
			rctns2Fire[stateIdx]=chsCount-1;
	}
	//my_assert(row[rctns2Fire[stateIdx]+1]-row[rctns2Fire[stateIdx]]>0);
}


__global__ void AdvanceTimeAndUpdatePropensitiesByThreadDirect(int realizationsCount, int chsCount, int rctsCount,
	const float *randomNumbers,
	CudaPitched2D<float> propensities,
	CudaPitched2D<int> population,
	AffectedChannels affectedChannels,
	float *totalPropSum)
{

	int idx=blockDim.x * blockIdx.x + threadIdx.x;

	if(idx>=realizationsCount)
		return;

	int rnToBeFired=(int)(randomNumbers[idx]*chsCount);//initial index of a reaction
	my_assert(rnToBeFired>=0&&rnToBeFired<chsCount);
	
	/*for(int i=0; i<chsCount; ++i){
		if(fabs(ReactionPropensity(sortedIndices[i], population.Row(idx))-propensities.Row(idx)[i])>1e-5)
			printf("before %d %d %d %e %e %e\n", 
			idx, i, sortedIndices[i], ReactionPropensity(backIndices[i], population.Row(idx)), ReactionPropensity(sortedIndices[i], population.Row(idx)), propensities.Row(idx)[i]);
	}*/

	int const *affectedReactions=affectedChannels.AffectedRow(rnToBeFired);
	int len=affectedChannels.RowLen(rnToBeFired);

	float oldPropSum=0.f;
	for(int i=0; i<len; ++i){
		oldPropSum+=ReactionPropensity(affectedReactions[i], population.Row(idx));
	}

	FireReaction(rnToBeFired, population.Row(idx));

	float newPropSum=0.f;
	for(int i=0; i<len; ++i){
		int const initialAffRnInd=affectedReactions[i];
		float prop=ReactionPropensity(initialAffRnInd, population.Row(idx));
		newPropSum+=prop;
		propensities.Row(idx)[initialAffRnInd]=prop;
	}

	/*for(int i=0; i<chsCount; ++i){
		if(fabs(ReactionPropensity(sortedIndices[i], population.Row(idx))-propensities.Row(idx)[i])>1e-5)
			printf("after %d %d %d %e %e\n", idx, i, backIndices[i], ReactionPropensity(sortedIndices[i], population.Row(idx)), propensities.Row(idx)[i]);
	}*/

	totalPropSum[idx]+=newPropSum-oldPropSum;
}

__global__ void AdvanceTimeAndUpdatePropensitiesByWarpDirect(int realizationsCount, int chsCount, int rctsCount,
	float *randomNumbers,
	CudaPitched2D<float> propensities,
	CudaPitched2D<int> population,
	AffectedChannels affectedChannels,
	float *totalPropSum)
{

	int idx=blockDim.x * blockIdx.x + threadIdx.x;

	int const realizationIdx=idx>>5;

	if(realizationIdx>=realizationsCount)
		return;

	int const inWarpIdx=idx&31;
	
	int warpInBlockIdx=realizationIdx%WARPS_IN_BLOCK;

	int rnToBeFired=(int)(randomNumbers[realizationIdx]*chsCount);//initial index of a reaction
	my_assert(rnToBeFired>=0&&rnToBeFired<chsCount);

	int const *affectedReactions=affectedChannels.AffectedRow(rnToBeFired);
	int len=affectedChannels.RowLen(rnToBeFired);

	my_assert(len<=warpSize);

	__shared__ float s_aux_for_scan[64*WARPS_IN_BLOCK];
	(s_aux_for_scan+64*warpInBlockIdx)[inWarpIdx]=0;

	float prop;
	if(inWarpIdx<len){
		prop=ReactionPropensity(affectedReactions[inWarpIdx], population.Row(realizationIdx));
	}else{
		prop=0;
	}
	__shared__ float oldPropSum;
	inclusive_warp_scan(inWarpIdx, prop, s_aux_for_scan+64*warpInBlockIdx);
	if(inWarpIdx==31)
		oldPropSum=(s_aux_for_scan+64*warpInBlockIdx)[63];

	//fire with at most 4 threads uesd
	FireReaction(inWarpIdx, rnToBeFired, population.Row(realizationIdx));//TODO: modify!


	if(inWarpIdx<len){
		int affectedReactionInd=affectedReactions[inWarpIdx];
		my_assert(affectedReactionInd>=0&&affectedReactionInd<chsCount);
		
		prop=ReactionPropensity(affectedReactionInd, population.Row(realizationIdx));

		propensities.Row(realizationIdx)[affectedReactionInd]=prop;
	}else{
		prop=0;
	}

	__shared__ float newPropSum;
	inclusive_warp_scan(inWarpIdx, prop, s_aux_for_scan+64*warpInBlockIdx);
	if(inWarpIdx==31)
		newPropSum=(s_aux_for_scan+64*warpInBlockIdx)[63];

	/*for(int i=0; i<chsCount; ++i){
		if(fabs(ReactionPropensity(i, population.Row(realizationIdx))-propensities.Row(realizationIdx)[i])>1e-5)
			printf("after %d %d %e %e\n", idx, i, ReactionPropensity(i, population.Row(realizationIdx)), propensities.Row(realizationIdx)[i]);
	}*/
	if(inWarpIdx==0)
		totalPropSum[realizationIdx]+=newPropSum-oldPropSum;
}


__global__ void AdvanceTimeAndUpdatePropensitiesByThreadIndirect(int realizationsCount, int chsCount, int rctsCount,
	const float *randomNumbers,
	CudaPitched2D<float> propensities,
	CudaPitched2D<int> population,
	int const *sortedIndices,//old indices of reactions in a sorted array
	int const *backIndices,
	AffectedChannels affectedChannels,
	float *totalPropSum)
{

	int idx=blockDim.x * blockIdx.x + threadIdx.x;

	if(idx>=realizationsCount)
		return;

	int rnToBeFired=backIndices[(int)(randomNumbers[idx]*chsCount)];//initial index of a reaction
	my_assert(rnToBeFired>=0&&rnToBeFired<chsCount);
	
	/*for(int i=0; i<chsCount; ++i){
		if(fabs(ReactionPropensity(sortedIndices[i], population.Row(idx))-propensities.Row(idx)[i])>1e-5)
			printf("before %d %d %d %e %e %e\n", 
			idx, i, sortedIndices[i], ReactionPropensity(backIndices[i], population.Row(idx)), ReactionPropensity(sortedIndices[i], population.Row(idx)), propensities.Row(idx)[i]);
	}*/

	int const *affectedReactions=affectedChannels.AffectedRow(rnToBeFired);
	int len=affectedChannels.RowLen(rnToBeFired);

	float oldPropSum=0.f;
	for(int i=0; i<len; ++i){
		oldPropSum+=ReactionPropensity(affectedReactions[i], population.Row(idx));
	}

	FireReaction(rnToBeFired, population.Row(idx));

	float newPropSum=0.f;
	for(int i=0; i<len; ++i){
		int const initialAffRnInd=affectedReactions[i];
		float prop=ReactionPropensity(initialAffRnInd, population.Row(idx));
		newPropSum+=prop;
		propensities.Row(idx)[backIndices[initialAffRnInd]]=prop;
	}

	/*for(int i=0; i<chsCount; ++i){
		if(fabs(ReactionPropensity(sortedIndices[i], population.Row(idx))-propensities.Row(idx)[i])>1e-5)
			printf("after %d %d %d %e %e\n", idx, i, backIndices[i], ReactionPropensity(sortedIndices[i], population.Row(idx)), propensities.Row(idx)[i]);
	}*/

	totalPropSum[idx]+=newPropSum-oldPropSum;
}


__global__ void AllPropensitiesKernel1(int chsCount, int statesCount, 
									  CudaPitched2D<int> reactantPopulation, 
									  CudaPitched2D<float> propensities,
									  int const *whereToStore, 
									  float *totalPropSum)
{
	PROLOG;
	if(stateIdx>=statesCount)
		return;
	
	//int offset=stateIdx*chsCount;
	/*my_assert(stateIdx<propensities.Height());
	float *rowProp=propensities.Row(stateIdx);
	int const *rowReactsPopulation=reactantPopulation.Row(stateIdx);
	rowProp[innerIdx]=ReactionPropensity(innerIdx, rowReactsPopulation);*/

	float *rowProp=propensities.Row(stateIdx);
	int const *rowReactsPopulation=reactantPopulation.Row(stateIdx);
	ComputePropByMP1(innerIdx, rowProp, rowReactsPopulation, chsCount, whereToStore);

	__shared__ double s_aux_for_scan[WARPS_IN_BLOCK*64];

	int warpInBlockIdx=stateIdx%WARPS_IN_BLOCK;

//	if(innerIdx<chsCount)
	//	printf("%d %f\n", innerIdx, rowProp[innerIdx]);

	float sum=SumOfArray(innerIdx, warpInBlockIdx, rowProp, chsCount, s_aux_for_scan+warpInBlockIdx*64);

	if(innerIdx==0)
		totalPropSum[stateIdx]=sum;

}


class BadPropFinder
{
	int const *m_popl;
	PropReal_t const *m_prps;
public:
	BadPropFinder(int const *popl, PropReal_t const *prps):m_popl(popl), m_prps(prps){}

	inline __device__
	bool operator()(int chInd)const
	{
		if (m_prps[chInd]<=0)
			return false;
		int const beginInd=REACTION_ROW_BEGIN(chInd);
		int const endInd=REACTION_ROW_BEGIN(chInd+1);

		for(int i=beginInd; i<endInd; ++i)
		{
			int ind=GET_REACTANT(i);
			char order;
			split(ind, order, ind);
			if(m_popl[ind]+order<0){
				printf("reaction %d, population=%d, order=%d, propensity=%f\n", chInd, m_popl[ind], order, m_prps[chInd]);
				printf("reactants: ");
				for(int j=beginInd; j<endInd; ++j)
				{
					int ind=GET_REACTANT(j);
					char order;
					split(ind, order, ind);
					printf("%d:%d ", order, ind);
				}
				printf("\n");
				return true;
			}else
				return false;
		}
		return true;
	}
	
};

void CheckPropensities(int statesCount, int chsCount)
{  
	
	for(int i=0; i<statesCount; ++i)
	{
		//thrust::device_ptr<int> thustPropensitiesPtr(d_propensities.Row(i));
		thrust::counting_iterator<int> cit(0);
		if(thrust::count_if(cit, cit+chsCount, BadPropFinder(d_reactantPopulation.Row(i), d_propensities.Row(i)))!=0)
			exit(1);
	}
		
}


template <bool getsReactionNumber, bool saveStatistics>
__global__ void AdvanceTimeAndUpdatePropensitiesByWarpIndirect(int realizationsCount, int chsCount, int rctsCount,
	uint4 * tauSeeds,
	float *timeArray,
	PropReal_t *randomNumbers,
	int const* reactionToFire,
	CudaPitched2D<PropReal_t> propensities,
	CudaPitched2D<int> prevPopulation,
	CudaPitched2D<int> population,
	int const *sortedIndices,//old indices of reactions in a sorted array
	int const *backIndices,
	AffectedChannels affectedChannels,
	PropSumReal_t *totalPropSum, int const *rangesInd, int rangesInArray,
	Statistics statistics)
{

	int idx=blockDim.x * blockIdx.x + threadIdx.x;

	int const realizationIdx=idx>>5;

	if(realizationIdx>=realizationsCount)
		return;

	int const inWarpIdx=idx&31;
	
	int warpInBlockIdx=realizationIdx%WARPS_IN_BLOCK;

	__shared__ float timeStep[WARPS_IN_BLOCK];//time steps for warps in a block
	uint4 seed;//it's a bit faster when seeds are copied to registers first
	if(inWarpIdx==0){
		seed=tauSeeds[realizationIdx];
		timeStep[warpInBlockIdx]=-logf(HybridTausRngReduced(&seed))/totalPropSum[(realizationIdx+1)*rangesInArray-1];
	}

	//reaction to fire, index in a sorted array
	int rnInd=reactionToFire[realizationIdx];
	
	//may accidentally get a wrong reaction index
	if(rnInd<0||rnInd>chsCount){
		return;
	}

	//affected reactions
	int const *affectedReactions=affectedChannels.AffectedRow(rnToBeFired);
	int len=affectedChannels.RowLen(rnToBeFired);

	my_assert(len<=warpSize);

	//auxiliary array for scanning
	__shared__ PropSumReal_t s_aux_for_scan[64*WARPS_IN_BLOCK];
	(s_aux_for_scan+64*warpInBlockIdx)[inWarpIdx]=0;

	
	PropSumReal_t prop;//affected reaction propensity
	int affctRn;
	if(inWarpIdx<len){
		affctRn=affectedReactions[inWarpIdx];
		prop=ReactionPropensity(affctRn, population.Row(realizationIdx));
		affctRn=backIndices[affctRn];//it's original index
	}else{//should have valid propensities for all threads in a warp
		prop=0;
		affctRn=chsCount+1;
	}

	PropSumReal_t *oldPropSum=s_ext_shared;
		
	//compute old ranges' cumulative partial sums,
	//the most "suspicious" part of the algorithm
	for(int i=0; i<rangesInArray; ++i){
		UseInPartialSum predicate(rangesInd[i]);
		inclusive_warp_scan(inWarpIdx, aprop, affctRn, predicate, s_aux_for_scan+64*warpInBlockIdx);
		if(inWarpIdx==31)
			oldPropSum[warpInBlockIdx*rangesInArray+i]=(s_aux_for_scan+64*warpInBlockIdx)[63];
	}

	//fire with at most 4 threads used
	FireReaction(inWarpIdx, rnToBeFired, population.Row(realizationIdx));

	//get new propensities for affected reactions...
	if(inWarpIdx<len){
		affctRn=affectedReactions[inWarpIdx];
		my_assert(affctRn>=0&&affctRn<chsCount);
		
		prop=ReactionPropensity(affctRn, population.Row(realizationIdx));
		affctRn=backIndices[affctRn];

		propensities.Row(realizationIdx)[affctRn]=prop;
	}else{
		prop=0;
		affctRn=chsCount+1;
	}

	//compute old ranges' cumulative partial sums,
	//the most "suspicious" part of the algorithm
	PropSumReal_t *newPropSum=oldPropSum+rangesInArray*WARPS_IN_BLOCK;
	for(int i=0; i<rangesInArray; ++i){
		UseInPartialSum predicate(rangesInd[i]);
		inclusive_warp_scan(inWarpIdx, prop, affctRn, predicate, s_aux_for_scan+64*warpInBlockIdx);
		if(inWarpIdx==31)
			newPropSum[warpInBlockIdx*rangesInArray+i]=(s_aux_for_scan+64*warpInBlockIdx)[63];
	}
	
	//put computed data back into global memory
	if(inWarpIdx==0){
		tauSeeds[realizationIdx]=seed;
		for(int i=0; i<rangesInArray; ++i){
			totalPropSum[realizationIdx*rangesInArray+i]+=newPropSum[warpInBlockIdx*rangesInArray+i]-oldPropSum[warpInBlockIdx*rangesInArray+i];
		}
		timeArray[realizationIdx]+=timeStep[warpInBlockIdx];
	}
}