#pragma once
#include "realtype.h"
#include "CudaPitched2D.h"

void FindReactionToBeFired(int realizationsCount, int chsCount, 
							   uint4 *rSeeds, //seeds for random reaction selection
							   CudaPitched2D<PropReal_t> propensities, //chsCount x realizationsCount
							   PropSumReal_t const *totalPropSum, //rangesInArray x realizationsCount
							   int predictedIndex, //length of searching chunk
							   int const *rangesIndices, int rangesInArray, //subrange ends' indices, subranges count
							   int *reactionsToFire, //what reaction to fire
							   PropReal_t *val2Find,//not used when storeValue2Find==false
							   bool storeValuesToFind//store or not values to find
							   );