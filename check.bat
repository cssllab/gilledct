@echo off
cls
setlocal
path="../debug/";%path% 
echo on
gillidrct.exe -f ../data/dimer_decay.xml -n 100 -t 7 -l 10
gillidrct.exe -f ../data/test_16.xml -n 100 -t 0.01 -l 10
gillidrct.exe -f ../data/test_20.xml -n 100 -t 0.01 -l 10
gillidrct.exe -f ../data/test_32.xml -n 100 -t 0.01 -l 10
gillidrct.exe -f ../data/test_50.xml -n 1000 -t 0.01 -l 10
gillidrct.exe -f ../data/test_100.xml -n 100 -t 0.01 -l 10