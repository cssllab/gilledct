#pragma once
#include "filereader.h"
class LolCatReader //:public FileReader
{
public:
	LolCatReader(void);
	~LolCatReader(void);
	static void Read(const char *channelsFN, const char *speciesFN, 
		std::vector<ReactionProp> &reactionProps, 
		FileReader::ReactantsPopulations_t &reactantsPopulations);

private:
	static void ReadChannels(std::ifstream &ifile, std::vector<ReactionProp> &reactionProps);
	static void ReadSpecies (std::ifstream &ifile, FileReader::ReactantsPopulations_t &reactantsPopulations);
};

