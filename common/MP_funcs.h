#pragma once

#include "const.h"
#include "div_up.h"
#include "functions.h"

__host__ inline
int PaddedLengthForPrefixSum(int sz)
{
	const int THREADS_IN_WARP=32;
	if(sz<=THREADS_IN_WARP*2)
		return nextPowerOf2(sz);
	else
	{
		
		int chunks=DIV_UP(sz, THREADS_IN_WARP*2);
		//printf("sz=%d; chunks=%d, res=%d\n", sz, chunks, chunks*THREADS_IN_WARP*2);
		return chunks*THREADS_IN_WARP*2;
	}
}