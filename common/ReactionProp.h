#pragma once

#include <unordered_map>
#include <string>

#if defined _WIN32 && _MSC_VER<1600
namespace std{
	using namespace tr1;
}
#endif

//auxilary structure
struct ReactionProp
{
	float rate;
	std::unordered_map<std::string, char> reactants;
	std::unordered_map<std::string, char> products;
};

inline
bool operator ==(ReactionProp const &lhs, ReactionProp const &rhs){
	return (fabs(lhs.rate-rhs.rate)<1e5)&&
		(std::equal(lhs.reactants.begin(), lhs.reactants.end(), rhs.reactants.begin()))&&
		(std::equal(lhs.products.begin(), lhs.products.end(), rhs.products.begin()));
}