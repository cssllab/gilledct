#pragma once

#include "FileReader.h"

//class for reading reaction network out of "StochKit" XML file

#include <rapidxml/rapidxml.hpp>


class StochKitReader:public FileReader
{
public:
	StochKitReader(void);
	~StochKitReader(void);
	void ReadXML(const char *fn, std::vector<ReactionProp> &reactionProps,
		ReactantsPopulations_t &reactantsPopulations)const;

private:
		void ReadReactionsList(rapidxml::xml_node<> *modelNode,
			std::unordered_map<std::string, float> const &rIdConstMap,
			std::vector<ReactionProp> &reactionProps) const;
		void ReadReactantsPopulations(rapidxml::xml_node<> *modelNode,
			std::unordered_map<std::string, std::pair<int, int> > &reactantsPopulations) const;

		void ReadReactionsRates(rapidxml::xml_node<> *modelNode, std::unordered_map<std::string, float>& rIdConstMap)const;
};
