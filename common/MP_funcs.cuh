#pragma once

#include "defines.cuh"
#include "functions.h"

#include "MP_funcs.h"
#include "realtype.h"


//needs all 32 threads in a warp to run this function
//val - is an input value for the current thread
//takes extra array of 64 elements in a shared memory to perform summatiom
template <typename T>
__forceinline__
__device__ T inclusive_warp_scan(int idx, T val, volatile T *s_data)
{
	assert(idx<warpSize);
	assert(s_data[idx]==(T)0);
	
//	s_data[idx]=0.;
	idx+=warpSize;
	assert(idx<warpSize*2);
	T t=s_data[idx]=val;
	
#ifndef NDEBUG
	if(idx==32){
		for(int i=0; i<warpSize; ++i){
			if(s_data[i]!=0.)
				printf("error in inclusive_warp_scan: must be zeros %d, %e\n", i, s_data[i]);
		}
	}
#endif

	s_data[idx]=t=t+s_data[idx-1];
	s_data[idx]=t=t+s_data[idx-2];
	s_data[idx]=t=t+s_data[idx-4];
	s_data[idx]=t=t+s_data[idx-8];
	s_data[idx]=t=t+s_data[idx-16];
	return s_data[idx];
}

//needs all 32 threads in a warp to run this function
//val - is an input value for the current thread
//takes extra array of 64 elements in a shared memory to perform summatiom
template <typename T, typename Predicate>
__forceinline__
__device__ T inclusive_warp_scan(int idx, T val, int rnInd, Predicate p, volatile T *s_data)
{
	assert(idx<warpSize);
	assert(s_data[idx]==(T)0);
	
//	s_data[idx]=0.;
	idx+=warpSize;
	assert(idx<warpSize*2);
	T t;
	if(p(rnInd))
		t=s_data[idx]=val;
	else
		t=s_data[idx]=0;
	
#ifndef NDEBUG
	if(idx==32){
		for(int i=0; i<warpSize; ++i){
			if(s_data[i]!=0.)
				printf("error in inclusive_warp_scan: must be zeros %d, %e\n", i, s_data[i]);
		}
	}
#endif

	s_data[idx]=t=t+s_data[idx-1];
	s_data[idx]=t=t+s_data[idx-2];
	s_data[idx]=t=t+s_data[idx-4];
	s_data[idx]=t=t+s_data[idx-8];
	s_data[idx]=t=t+s_data[idx-16];
	return s_data[idx];
}


template <typename T>
__forceinline__
__device__ T inclusive_warp_scan_alt(int idx, T val, volatile T *s_data)
{
	assert(idx<warpSize);

	s_data[idx] = val;

    if (idx >=  1)  s_data[idx] = val = s_data[idx -  1]+ val;
    if (idx >=  2)  s_data[idx] = val = s_data[idx -  2]+ val;
    if (idx >=  4)  s_data[idx] = val = s_data[idx -  4]+ val;
    if (idx >=  8)  s_data[idx] = val = s_data[idx -  8]+ val;
    if (idx >= 16)  s_data[idx] = val = s_data[idx - 16]+ val;

    return val;
}


//needs all 32 threads in a warp to run this function
//val - is an input value for the current thread
//takes extra array of 64 elements in a shared memory to perform summatiom
template <typename T>
inline
__device__ T inclusive_warp_scan_1(int idx, T val, volatile T *s_data)
{
	idx+=warpSize;
	T t=s_data[idx]=val;

	s_data[idx]=t=t+s_data[idx-1];
	s_data[idx]=t=t+s_data[idx-2];
	s_data[idx]=t=t+s_data[idx-4];
	s_data[idx]=t=t+s_data[idx-8];
	s_data[idx]=t=t+s_data[idx-16];
	return s_data[idx];
}


__device__ inline
int WarpChunks(int len)
{
	assert(warpSize==32);
	return (len&(31))?((len>>5)+1):(len>>5);
}

/*template <typename T>
__device__ void WarpInclusivePrefixSum(int inWarpThreadIdx, T *arr, int len, T volatile *sharedArr)
{
	int chunks=WarpChunks(len);

	T add=0.f;
	for(int ch=0;;)
	{
		int indInArr=inWarpThreadIdx+ch*warpSize;
		
		T input=(indInArr<len)?arr[indInArr]:0.f;
		T dummy;
		T &output=(indInArr<len)?arr[indInArr]:dummy;
				
		output=inclusive_warp_scan(inWarpThreadIdx, input, sharedArr)+add;
		//printf("PrefixScanWarp: %f\n",currArr[indInChunk]);

		++ch;
		if(ch>=chunks)
			break;
		else{
			add=arr[ch*warpSize-1];
			//len-=WARP_SIZE;
		}

	}
}*/

template <typename T>
__device__ void WarpInclusivePrefixSum(int inWarpThreadIdx, T const *inArr, T *outArr, int len, T volatile *sharedArr)
{
	int chunks=WarpChunks(len);
	/*__shared__ T add;
	if(inWarpThreadIdx==31)
		add=0;*/
	T add=0;

	//T add=0.f;//add this value to every element in a chunk
	for(int ch=0;;)//iterate through multiple chunks
	{
		int indInArr=inWarpThreadIdx+ch*warpSize;
		
		//we need all 32 threads in a warp working on this chunk, 
		//so we need dummy values to prevent memory access beyong the arrays' blundaries
		T input=(indInArr<len)?inArr[indInArr]:0.f;
		T dummy;
		T &output=(indInArr<len)?outArr[indInArr]:dummy;
				
		output=inclusive_warp_scan(inWarpThreadIdx, input, sharedArr)+add;
		//break;
		//printf("PrefixScanWarp: %f\n",currArr[indInChunk]);

		++ch;
		if(ch>=chunks)
			break;
		else{
			//if(inWarpThreadIdx==31)
				add=outArr[ch*warpSize-1];
		}

	}
}


template <typename T> __device__ inline 
int FirstThreadIdThatGE(int inWarpThreadIdx, T const *arr, T toFind, int chunkLen)
{
	//assert(chunkLen<=warpSize);

	int predicate=((inWarpThreadIdx<chunkLen)?arr[inWarpThreadIdx]:-FLT_MAX)>=toFind;
	unsigned int blt=__ballot(predicate);
	//printf("threadIdx: %d, predicate: %u\n", idx, predicate);
//	if(idx==0)
	//	printf("__ballot(predicate): %u\n", blt);
	int ffs=__ffs(blt);
	//if(idx==0)
	//	printf("__ffs(blt): %d\n", ffs);
	return ffs-1;
}


//TODO: remove from here?
template <typename T> __device__ __forceinline__
int FirstThreadIdThatGE(T threadVal, T toFind)
{
	int predicate=threadVal>=toFind;
	unsigned int blt=__ballot(predicate);
	return __ffs(blt)-1;
}

template <typename T> __device__ inline 
int WarpFindGEInArr(int inWarpThreadIdx, T const *arr, int len, T toFind/*, int *inChunk*/)
{
	int chunks=WarpChunks(len);
	int loChunk=0;
	int hiChunk=chunks;
	while(loChunk!=hiChunk){
		int chunkId=(loChunk+hiChunk)/2;
		int shift=chunkId*warpSize;
		PropReal_t const* currArr=arr+shift;
		int posInChunk=FirstThreadIdThatGE(inWarpThreadIdx, currArr, toFind, len-shift);//<0 - all less; ==0 all greater; >0 - some greater

		if(posInChunk<0){//not found here nor to the left
			loChunk=chunkId+1;
		}else if(posInChunk==0)	{//found here or to the left
			if(chunkId==0||currArr[-1]<toFind){
			//	*inChunk=chunkId;
				return shift;
			}
			else
				hiChunk=chunkId;
		}else{// (posInChunk>0)
		//	*inChunk=chunkId;
			return posInChunk+shift;
		}
	}
	//if(inWarpThreadIdx==0)
		//printf("tried to find %f but failed\n", toFind);
	//assert(false);
	//*inChunk=-1;
	return -1;
}



//use one MP for computing an inclusive prefix_scan over the arr
//n must be a power of 2 and no more than 64
template <typename T>
__device__ inline
void PrefixSumByMPChunk(int idx, int n, T *arr)
{
	assert(idx<THREADS_IN_WARP);
	assert(n<=THREADS_IN_WARP*2);
	
	if(idx*2>=n)
		return;

	//assert(n<=2*THREADS_IN_WARP&&"not implemented yet");

	T saved=arr[n-2];
	int offset=1;
	
	//if(idx==0) 
	//	printf("n=%d\n", n);
	for (int d = n>>1; d > 0; d >>= 1) // build sum in place up the tree 
    {
		//if(idx==0)
		//	printf("d=%d\n", d);
		if(idx<d)
		{
			int ai=offset*(2*idx+1)-1;
			int bi=offset*(2*idx+2)-1;
			 
			//printf("before: idx=%d, arr[%d]=%f, arr[%d]=%f\n", idx, ai, arr[ai], bi, arr[bi]);
			arr[bi]+=arr[ai];
			//printf("after: idx=%d, arr[%d]=%f, arr[%d]=%f\n", idx, ai, arr[ai], bi, arr[bi]);
			//printf("i1=%d, i2=%d\n", bi, ai);
		}
		offset*=2;
	}

	if(idx==0)
	{
		arr[n-1]=0;
	}

	for(int d=1; d<n; d*=2)
	{
		offset>>=1;
		if(idx<d)
		{
			int ai=offset*(2*idx+1)-1;
			int bi=offset*(2*idx+2)-1;

			T t=arr[ai];
			arr[ai]=arr[bi];
			arr[bi]+=t;
			//printf("i1=%d, i2=%d\n", bi, ai);
		}
	}
	assert(arr[0]==0);

	if(idx==0)
	{
		arr[n-1]=arr[n-2]+saved;
		//printf("i1=%d, i2=%d\n", n-1, n-2);
	}
}

template <typename T>
__device__ inline
void PrefixSumByMP(int idx, int warpInBlockId, int sz, T *arr)
{
	sz+=1;//well, I don't know where does it come from...
	assert(idx<warpSize);

	if(idx>=sz)
		return;

	int chunks=DIV_UP(sz, warpSize*2);
	//if(idx==0)
	//	printf("sz=%d, use %d chunks\n", sz, chunks);

	__shared__ PropReal_t prev_add[WARPS_IN_BLOCK];
	if(idx==0)
	{
		prev_add[warpInBlockId]=0;
	}

	for(int i=0; i<chunks; ++i)
	{
		__shared__ PropReal_t curr_add[WARPS_IN_BLOCK];
		
		if(idx==0)
		{
			int ind=warpSize*2*(i+1)-1;
			if(ind<sz)
				curr_add[warpInBlockId]=arr[ind];
			//printf("i=%d, curr. addition: %f\n", i, curr_add);
		}

		int chunkSz=min(nextPowerOf2(sz), warpSize*2);
		//if(idx==0)
		//	printf("sz=%d\n",chunkSz);
		//assert(false);

		PrefixSumByMPChunk(idx, chunkSz, arr+warpSize*2*i);
		if(idx==0)
		{
			int ind=warpSize*2*(i+1)-1;
			if(ind<sz)
				curr_add[warpInBlockId]+=arr[ind]+prev_add[warpInBlockId];
			//printf("i=%d, addition: %f, curr: %f\n", i, prev_add, curr_add);
		}

		
		int ind1=idx+warpSize*2*i;
		int ind2=idx+warpSize*(2*i+1);

		//	arr[idx+THREADS_IN_WARP*2*i]+=prev_add;
		//	printf("i1=%d\n", idx+THREADS_IN_WARP*2*i);
		if(ind1<sz)
		{
			arr[ind1]+=prev_add[warpInBlockId];
		//	printf("i2=%d\n", ind1);
		}
		if(ind2<sz)
		{
			arr[ind2]+=prev_add[warpInBlockId];
			//printf("i2=%d\n", ind2);
		}

		

		if(idx==0)
			prev_add[warpInBlockId]=curr_add[warpInBlockId];

	}
}


__device__ inline void FindFirstGreaterThanMP(int idx, int len, PropReal_t const *arr, PropReal_t val2Find,
										int *pos)
{ 
	assert(idx<warpSize);

	int chunkSize=DIV_UP(len, warpSize);
	int beginIndex=idx*chunkSize;
	int endIndex=chunkSize*(idx+1);

	if(beginIndex>=len)
		return;

	if(endIndex>=len)
		endIndex=len;

	if(idx==0)
	{
		//printf("looking for %f in array of size %d, chunkSize=%d\n", val2Find, len, chunkSize);
		if(arr[0]>val2Find)
		{
			*pos=0;
			return;
		}
		if(chunkSize==1)
			return;
	} else if(chunkSize==1)//for chunkSize==1 case
	{
		//printf("arr[%d]=%f, arr[%d-1]=%f\n", 
		//	beginIndex, arr[beginIndex], beginIndex, arr[beginIndex-1]);
		if(arr[beginIndex-1]<=val2Find&&arr[beginIndex]>val2Find)
			*pos=beginIndex;
		return;
	}

	int middleIndex=-1;
	while(endIndex-beginIndex>1)
	{
		if(beginIndex!=0)
		{
			if(arr[beginIndex-1]>val2Find)
			{
				//printf("dropped on lhs: idx=%d, arr[%d-1]=%f\n",idx, beginIndex, arr[beginIndex-1]);
				return;
			}
		}
		
		if(endIndex<len-1)
		{
			if(arr[endIndex-1]<=val2Find)
			{
				//printf("dropped on rhs: idx=%d, arr[%d+1]=%f\n", idx, endIndex-1, arr[endIndex]);
				return;
			}
			
		}
		if(arr[beginIndex]>val2Find)
		{
			middleIndex=beginIndex;
			break;
		}

		//printf("remain: %d, arr[%d-1]=%f, arr[%d+1]=%f\n", idx, beginIndex, arr[beginIndex-1], endIndex-1, arr[endIndex]);

		middleIndex=(endIndex+beginIndex)/2;
		//printf("arr[%d]=%f\n", middleIndex, arr[middleIndex]);

		if(arr[middleIndex]<=val2Find)
		{
			beginIndex=middleIndex;
		}
		else
		{
			if(arr[middleIndex-1]<=val2Find)
			{
				//printf("gotcha2!\n");
				break;
			}
			endIndex=middleIndex;
		}
	}
	
	//printf("idx=%d, middleIndex=%d\n", idx, middleIndex);
	*pos=middleIndex;

	if(idx==0)
	{
		assert(arr[*pos]>val2Find);
		if(*pos!=0)
			assert(arr[(*pos)-1]<=val2Find);
	}

}

template <typename T>
__device__ inline 
void CopyArrayByMP(int idx, T *dst, T const *src, int len)
{
	/*int chunkSize=DIV_UP(len, THREADS_IN_WARP);

	int start=idx*chunkSize;
	int finish=(idx+1)*chunkSize;
	for(int i=start; i<finish; ++i)
	{
		if(i<len)
		{
			//printf("id=%d, start=%d, finish=%d, len=%d\n", idx, start, finish, len);
			dst[i]=src[i];
			//printf("i=%d\n", i);
		}
	}*/
	int chunkSize=DIV_UP(len, warpSize);
	if(chunkSize==0)
		++chunkSize;

	//printf("len=%d, id=%d, chunkSize=%d\n", len, idx, chunkSize);
	for(int i=0; i<chunkSize; ++i)
	{
		int steppedI=idx+warpSize*i;
		if(steppedI<len)
		{
			//printf("id=%d, len=%d, steppedI=%d\n", idx, len, steppedI);
			dst[steppedI]=src[steppedI];
			//printf("i=%d\n", i);
		}
	}
}

template <typename T>
__device__ inline 
void CopyArrayByMP(int idx, PropReal_t *dst, T const *prevSrc, T const *src, int len, PropReal_t timeStepRatio)
{
   // assert(0<=timeStepRatio&&timeStepRatio<=1);
	/*int chunkSize=DIV_UP(len, THREADS_IN_WARP);

	int start=idx*chunkSize;
	int finish=(idx+1)*chunkSize;
	for(int i=start; i<finish; ++i)
	{
		if(i<len)
		{
			//printf("id=%d, start=%d, finish=%d, len=%d\n", idx, start, finish, len);
			dst[i]=src[i];
			//printf("i=%d\n", i);
		}
	}*/
	int chunkSize=DIV_UP(len, warpSize);
	if(chunkSize==0)
		++chunkSize;

	//printf("len=%d, id=%d, chunkSize=%d\n", len, idx, chunkSize);
	for(int i=0; i<chunkSize; ++i)
	{
		int steppedI=idx+warpSize*i;
		if(steppedI<len)
		{
			//printf("id=%d, len=%d, steppedI=%d\n", idx, len, steppedI);
		//	if(prevSrc[steppedI]!= src[steppedI])
			//	printf("qq %d %d %d %f %f\n", steppedI, prevSrc[steppedI], src[steppedI], timeStepRatio, prevSrc[steppedI]+(src[steppedI]-prevSrc[steppedI])*timeStepRatio);
			dst[steppedI]=prevSrc[steppedI]+(src[steppedI]-prevSrc[steppedI])*timeStepRatio;
			//printf("%f \n", timeStepRatio);
			//if(src[steppedI]!=prevSrc[steppedI])
				//printf("%d %d\n", src[steppedI], prevSrc[steppedI]);
			//printf("i=%d\n", i);
		}
	}
}