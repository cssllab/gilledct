#pragma once

#include "const.h"


inline __device__ int Idx()
{
	return blockDim.x * blockIdx.x + threadIdx.x;
}

#define PROLOG \
	int idx=Idx();\
	int stateIdx=idx>>5;\
	int innerIdx=idx&31;
