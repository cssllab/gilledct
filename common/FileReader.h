#pragma once

#include "ReactionProp.h"
#include <vector>
#include <rapidxml/rapidxml.hpp>

class FileReader
{
public:
	typedef std::unordered_map<std::string, std::pair<int, int> > ReactantsPopulations_t;

	FileReader(void){}
	virtual ~FileReader(void){}

	void virtual ReadXML(const char *fn, std::vector<ReactionProp> &reactionProps,
		ReactantsPopulations_t &reactantsPopulations)const=0;
protected:
	static rapidxml::xml_node<> *FirstNode(rapidxml::xml_node<> *parentNode, const char *nodeName, bool throwOnError)
	{
		rapidxml::xml_node<> *node=parentNode->first_node(nodeName);
		if(node==NULL&&throwOnError)
		{
			char err[250];
			sprintf(err, "FileReader::readNode: can not find %s node", nodeName);
			throw std::runtime_error(err);
		}
		return node;
	}
};

