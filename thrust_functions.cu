#include <thrust/device_vector.h>
#include <thrust/iterator/counting_iterator.h>

using thrust::counting_iterator;

void testThrust(int size){
	counting_iterator<int> cit(0);
	thrust::device_vector<int> aux(size);
	thrust::copy(cit, cit+size, aux.begin());
}