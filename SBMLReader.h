#pragma once

#include "FileReader.h"

#include <rapidxml/rapidxml.hpp>

class SBMLReader:public FileReader
{
private:
	void ReadSpecies(rapidxml::xml_node<> *modelNode, 
					 ReactantsPopulations_t &reactantsPopulations)const;
	void ReadParameters(rapidxml::xml_node<> *modelNode, 
		std::unordered_map<std::string, float> &reactionParameters)const;
public:
	SBMLReader(void);
	~SBMLReader(void);
	void ReadXML(const char *fn, std::vector<ReactionProp> &reactionProps,
		ReactantsPopulations_t &reactantsPopulations)const;
};

