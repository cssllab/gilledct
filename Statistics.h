#pragma once

#include <thrust/iterator/constant_iterator.h>
#include <stdexcept>
#include "MP_funcs.cuh"
#include "CudaPitched3D.h"
#include "CummStat.h"
#include "defines.h"

class Statistics
{
	CudaPitched3D<PropReal_t> m_Populations;
	float *m_NextTimeToStore;
	int *m_NextTimeIndToStore;
	float m_logTimeStep;

public:

	__host__ __device__ inline
	int Reactants()const
	{
		return m_Populations.Width();
	}

	void Init(int numOfReactants, int numOfSystems, int numOfSteps, float logTimeStep)
	{
		std::cout<<"Allocating "<<(numOfSystems/(1024.*1024.))*numOfSteps*numOfReactants*sizeof(PropReal_t)<<" MB of memory for statistics... ";
		m_logTimeStep=logTimeStep;
		m_Populations.Init(numOfReactants, numOfSystems, numOfSteps);

		CHECK_CUDA_ERRORS();

		if(cudaMalloc((void**)&m_NextTimeToStore, sizeof(*m_NextTimeToStore)*numOfSystems)!=cudaSuccess)
			throw std::runtime_error("can not allocate Statistics");

		if(cudaMalloc((void**)&m_NextTimeIndToStore, sizeof(int)*numOfSystems)!=cudaSuccess)
			throw std::runtime_error("can not allocate Statistics");

		thrust::constant_iterator<float> citf(logTimeStep);
		thrust::copy(citf, citf+numOfSystems, 
			thrust::device_pointer_cast(m_NextTimeToStore));
		CHECK_CUDA_ERRORS();

		thrust::constant_iterator<int> citi(1);
		thrust::copy(citi, citi+numOfSystems, 
			thrust::device_pointer_cast(m_NextTimeIndToStore));
		CHECK_CUDA_ERRORS();
		std::cout<<"done\n";
	}

	__device__ float NextTime(int r)const
	{
		return m_NextTimeToStore[r];
	}

	__device__ bool StorePopulationMP(int innerIdx, int stateIdx, 
		int const *prevPopulation, int const *population, float timeStepRatio)
	{
		int step2Save=m_NextTimeIndToStore[stateIdx];
		if(step2Save>=Steps())
		{
			return false;
		}

		PropReal_t *arrWhere2Save=m_Populations.Row(stateIdx, step2Save);

		CopyArrayByMP(innerIdx, arrWhere2Save, prevPopulation, population, Reactants(), timeStepRatio);

		if(innerIdx==0)
		{
			++m_NextTimeIndToStore[stateIdx];
			m_NextTimeToStore[stateIdx]+=m_logTimeStep;
		}
		return true;
	}

	__device__ __host__ inline
	const PropReal_t *Population(int systemId, int step)const
	{
		assert(systemId<Systems());
		assert(step<Steps());
		return m_Populations.Row(systemId, step);
	}

	__device__ __host__ inline
	PropReal_t *Population(int systemId, int step)
	{
		assert(systemId<Systems());
		assert(step<Steps());
		return m_Populations.Row(systemId, step);
	}

	void SetPopulation(CudaPitched2D<int> population, int step)
	{

		for(int i=0; i<Systems(); ++i)
			thrust::copy(thrust::device_pointer_cast(population.Row(i)), 
				thrust::device_pointer_cast(population.Row(i))+Reactants(), 
				thrust::device_pointer_cast(Population(i, step)));
	
	}

	__host__ __device__ inline
	int Steps()const
	{
		return m_Populations.Depth();
	}

	__host__ __device__ inline
	int Systems()const
	{
		return m_Populations.Height();
	}

	__host__
	float MinTime()const//TODO: move to device
	{
		return *thrust::min(thrust::device_pointer_cast(m_NextTimeToStore),
			thrust::device_pointer_cast(m_NextTimeToStore))-m_logTimeStep;
	}
	
	//Compute on the host side, rather ineffitient algorithm
	__host__ void Compute(std::vector<std::vector<CummStat>> &cummStat);
};
