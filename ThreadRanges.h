#pragma once

#include <thrust/device_vector.h>
#include "AffectedChannels.h"

void DetectThreadRanges(AffectedChannels affectedChannels, int channelsCount, thrust::device_vector<int> &elemsInRange, int rangesInArray, int maxListLen);