#pragma once
#include <thrust/functional.h>

struct IsValidRn:public thrust::unary_function<bool, int>
{
	int m_chsCount;
	IsValidRn(int chsCount):m_chsCount(chsCount){}

	__device__ inline
	int operator()(int rn)const
	{
		return rn<m_chsCount&&rn>=0;
	}
};