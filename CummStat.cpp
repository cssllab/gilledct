#include "CummStat.h"
#include <string>
#include <fstream>

using std::ofstream;
void print(std::vector<std::vector<CummStat>> const &cummStat, const char *char_base_fn, float totalTime)
{
	std::string base_fn=char_base_fn;
	ofstream fmin(base_fn+"_min.txt");
	ofstream fmax(base_fn+"_max.txt");
	ofstream fmean(base_fn+"_mean.txt");
	ofstream fstddev(base_fn+"_stddev.txt");

	for(size_t state=0; state<cummStat.size(); ++state)
	{
		auto const &rcts=cummStat[state];
		float time=totalTime*state/(cummStat.size()-1);
		fmin<<time<<"\t";
		fmax<<time<<"\t";
		fmean<<time<<"\t";
		fstddev<<time<<"\t";
		for(size_t rct=0; rct<rcts.size(); ++rct)
		{
			auto const &currCummStat=rcts[rct];
			fmin<<currCummStat.min<<"\t";
			fmax<<currCummStat.max<<"\t";
			fmean<<currCummStat.mean<<"\t";
			fstddev<<currCummStat.stddev<<"\t";
		}
		fmin<<"\n";
		fmax<<"\n";
		fmean<<"\n";
		fstddev<<"\n";
	}

}