#include "Statistics.h"
#include <thrust/extrema.h>
#include <thrust/iterator/transform_iterator.h>

struct CmpPopulations: public thrust::binary_function<int,int,bool>
{
	int m_r;
	int m_step;
	Statistics m_st;

	CmpPopulations(int r, int step, Statistics st):m_r(r), m_step(step), m_st(st)
	{
		assert(r<m_st.Reactants());
		assert(step<m_st.Steps());
	}
	__device__ inline
	bool operator()(int chId1, int chId2)const
	{
		assert(chId1<m_st.Systems());
		assert(chId2<m_st.Systems());
		//return chId1<chId2;//m_st.Population(chId, m_step)[m_r]; 
		return m_st.Population(chId1, m_step)[m_r]<m_st.Population(chId2, m_step)[m_r];
	}
};

struct Number2Population: public thrust::unary_function<int,float>
{
	int m_r;
	int m_step;
	Statistics m_st;

	Number2Population(int r, int step, Statistics st):m_r(r), m_step(step), m_st(st)
	{
		assert(r<m_st.Reactants());
		assert(step<m_st.Steps());
	}
	__device__ inline
	float operator()(int chId)const
	{
		assert(chId<m_st.Systems());
		//printf("sysId=%d, step=%d, r=%d, val=%f\n", chId, m_step, m_r, m_st.Population(chId, m_step)[m_r]);
		return m_st.Population(chId, m_step)[m_r]; 
	}
};

using thrust::counting_iterator;
using thrust::make_transform_iterator;
using std::cout;


struct StdDev:public thrust::unary_function<double, double>
{
	PropReal_t m_mean;

	StdDev(PropReal_t mean):m_mean(mean){}

	__device__ inline
	PropReal_t operator()(PropReal_t x)const{
		return (x-m_mean)*(x-m_mean);
	}
};

void Statistics::Compute(std::vector<std::vector<CummStat>> &cummStat)
{
	cummStat.resize(Steps());
	thrust::device_vector<double> aux(Systems());
	std::cout<<"Computing statistics...\n";
	for (int step=0; step<Steps(); ++step)
	{
		cummStat[step].resize(Reactants());
		for(int rct=0; rct<Reactants(); ++rct)
		{
			CmpPopulations fn(rct, step, *this);
			int systems=Systems();
			counting_iterator<int> cit(0);
		//	auto mme=thrust::minmax_element(cit, cit+Systems(), fn);
			//cout<<"mme "<<mme.first<<" "<<mme.second<<"\n";
			thrust::transform(cit, cit+Systems(), aux.begin(), Number2Population(rct, step, *this));
			auto mme2=thrust::minmax_element(aux.begin(), aux.end());

			double mean=thrust::reduce(aux.begin(), aux.end(), 0.0)/(double)Systems();


			double stddev=thrust::reduce(thrust::make_transform_iterator(aux.begin(), StdDev(mean)), 
				thrust::make_transform_iterator(aux.end(), StdDev(mean)), 0.0);
			stddev=sqrt(stddev/double(Systems()-1));

			//cout<<"mean: "<<mean<<"\n";
			CummStat &currCummStat=cummStat[step][rct];
			currCummStat.min=*mme2.first;
			currCummStat.max=*mme2.second;
			currCummStat.mean=mean;
			currCummStat.stddev=stddev;

		}
	}

}