///#include <stdio.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/iterator/counting_iterator.h>
//#include <conio.h>
#include <fstream>
#include <set>
#include <algorithm>

//#include <thrust/sort.h>

#include "defines.h"
#include "StochKitReader.h"
#include "SBMLReader.h"
#include "LolCatReader.h"
#include "CSCMatrix.h"
#include "CSRMatrix.h"
#include "CSRMatrixPacked.h"
#include "cl_parser.h"
#include "const.h"
#include "cl_parser.h"
#include "CSCHostMatrixData.h"
#include "MyTime.h"
#include "aux_funcs.h"
//#include "DataToConstMemory.h"
#include "propensities.h"
#include "control_vars.h"

#ifdef __CUDA_ARCH__
#endif

using std::cerr;
using std::cout;
using std::endl;
using std::numeric_limits;
using thrust::host_vector;
using thrust::counting_iterator;
using thrust::raw_pointer_cast;
#ifdef __WIN32
using std::tr1::unordered_set;
using std::tr1::unordered_map;
#else
using std::unordered_set;
using std::unordered_map;
#endif

using std::string;
using std::vector;

using std::auto_ptr;
using std::ofstream;
using std::flush;


bool statisticsEnabled=false;

extern AffectedChannels g_affectedChannels;

cusparseHandle_t g_handle;
std::vector<std::ofstream*> g_logFiles;

void ParseCL(int argc, char *argv[],
			 CLArguments &args, int &numLogIntervals)
{
    
	numLogIntervals=1;
	args.ai=numeric_limits<int>::max();
	ParseCL(argc, argv, args);

	for(int a=1; a<argc; ++a)
	{
		if(strcmp(argv[a],"-l")==0)//logging intervals
		{
			const char *ch=argv[++a];
			numLogIntervals=atoi(ch);
			continue;
		}else
		if(strcmp(argv[a],"-a")==0)//how many iterations to do before stop artificially
		{
			const char *ch=argv[++a];
			args.ai=atoi(ch);
			continue;
		}else if(strcmp(argv[a],"-s")==0)//prefix for statistics files
		{
			const char *ch=argv[++a];
			args.statPrefix=ch;
			continue;
		}
	}
}


void InitDevice(vector<ReactionProp> const &reactionProps,
	FileReader::ReactantsPopulations_t const &reactantsPopulations, 
	CLArguments const &args, int numLogIntervals, float logStep,
	std::string const STAT_DIR,
	int &reactantsCount, int &chsCounts, int &longestList,
	int *&reactionToFirePos,
	PropSumReal_t *&totalPropSum,
	PropReal_t *&randomNumbersCurr, PropReal_t *&randomNumbersNext)
{
	CSCHostMatrixData mData;
    host_vector<float> reactionConstants(reactionProps.size());
    host_vector<int> reactantsPopulation;
    vector<string> reactantNames;
	cout<<endl<<"Rearranging data structure... ";
	ReadXMLToMatrixStruct(reactionProps, reactantsPopulations,
        mData, reactantNames, reactantsPopulation, reactionConstants);

	cout<<"\t done"<<endl;

	//std::ofstream ofile("matrix.txt");
	//ofile<<mData;

	cout<<endl<<"Initializing data strucure in constant memory... ";

	DataToConstMemory(mData, reactionConstants, reactantNames.size());
	reactantsCount=reactantNames.size();
	chsCounts=reactionConstants.size();

	cout<<"\t done"<<endl;

	cout<<endl<<"Making assoc channel list... ";
	MakeAssocChannelsList(mData, chsCounts, reactantsCount, g_affectedChannels, longestList);
	cout<<"\t done"<<endl;
		
	cout<<endl<<"Initializing device arrays... ";
	InitDeviceArrays(chsCounts, reactantsPopulation, args.numParallel, 
		numLogIntervals+1, logStep, g_elementsInChunk, !args.statPrefix.empty());
	cout<<"\t done"<<endl;
		
	//channelReactionsListSize=mData.rowInd.size();

	cout<<endl<<"Initializing random seeds... ";
	CreateRandomSeedsUint4(args.numParallel);
	cout<<"\t done"<<endl;
		
	if(statisticsEnabled)
	{
		g_logFiles.resize(reactantNames.size());
		for(size_t i=0;i<g_logFiles.size();++i)
		{
			g_logFiles[i]=new std::ofstream();
			g_logFiles[i]->open((STAT_DIR+reactantNames[i]).c_str(), std::ios::trunc);
			assert(g_logFiles[i]->is_open());
			*g_logFiles[i]<<"#\ttime\tmin\tavg\tmax"<<endl;
		}
	}

	cout<<endl<<"Initial computing of propensities... ";
	InitSortedIndicesAndPropSum(args.numParallel, chsCounts, g_rangesInArray,
		reactionToFirePos, totalPropSum, randomNumbersCurr, randomNumbersNext
		//, additions, foundInRange
		);
	cout<<"\t done"<<endl;
}

void PrintConfig(CLArguments const &args, int numLogIntervals)
{
		cout<<"\n-------------**************------------\n";
	//cerr<<"-------------**************------------"

	cudaDeviceProp devProp;
    cudaGetDeviceProperties(&devProp, args.cudaDeviceNumber);

	cout<<"Running with parameters:\n";
	cout<<"\t"<<args.numParallel<<" realizations in parallel\n";
	cout<<"\t"<<args.endTime<<" end time\n";
	cout<<"\t"<<numLogIntervals<<" logging intervals\n";
	cout<<"\t"<<args.ai<<" artifitial iterations limit\n";
	cout<<"\t"<<WARPS_IN_BLOCK<<" warps per block\n";
	cout<<"\t"<<args.cudaDeviceNumber<<" cuda device # to use ("<<devProp.name<<")\n";
	cout<<"\t"<<g_rangesInArray<<" ranges in array used\n";
	cout<<"\t"<<g_elementsInChunk<<" elements in a chunk\n";
	if(!args.statPrefix.empty())
		cout<<"\t"<<"Statistics collection requested, file prefix: "<<args.statPrefix<<endl;
	else
		cout<<"\t"<<"Statistics is not requested to be collected."<<endl;

#ifdef USE_TEXTURES
	cout<<"\ttextures used"<<endl;
#endif

}

void CheckParams(CLArguments const &args)
{
	if(cudaSetDevice(args.cudaDeviceNumber)!=cudaSuccess){
		cerr<<"Can't use a GPU device #"<<args.cudaDeviceNumber<<"\n";
		exit(EXIT_FAILURE);
	}
	
	if(args.fileName.empty())
    {
        cerr<<"Specify input file name: <-f file name>\n"<<endl;
        exit(EXIT_SUCCESS);
    }

	if(!args.statPrefix.empty()&&!args.endTimeSet){
		cerr<<"Collecting statistics is not possible unless end time (-t key) is specified\n"<<endl;
        exit(EXIT_SUCCESS);
	}

	assert(!args.fileName.empty()&&"there must be a valid file name");
}

int main(int argc, char *argv[])
{
	std::string const STAT_DIR="stat//";

	const int numberOfStreams=1;

	int numLogIntervals=1;
	CLArguments args;
    ParseCL(argc, argv,
            args, numLogIntervals);

	float logStep=args.endTime/numLogIntervals;
	bool collectStatistics=!args.statPrefix.empty();

	PrintConfig(args, numLogIntervals);

	CheckParams(args);

	int const stepsWithoutRecomputingAndResorting=100000;

    vector<ReactionProp> reactionProps;
	FileReader::ReactantsPopulations_t reactantsPopulations;
		
	cout<<"Reading input file "<<args.fileName<<" ..."<<endl;
    try
    {
        StochKitReader skr;
		//SBMLReader skr;
		skr.ReadXML(args.fileName.c_str(), reactionProps, reactantsPopulations);
		//LolCatReader::Read(args.fileName.c_str(), args.fileName1.c_str(), reactionProps, reactantsPopulations);

		if(reactionProps.empty()){
			cerr<<"No reactions found in the file\n";
			exit(0);
		}

		if(reactantsPopulations.empty()){
			cerr<<"No reactions found in the file\n";
			exit(0);
		}

    }catch(std::exception const &ec)
    {
        cerr<<"Cannot read the file: "<<args.fileName<<"\treason: "<<ec.what()<<endl;
        exit(EXIT_FAILURE);
    }
    cout<<"Model has been read successfully"<<endl;
	cout<<"Channels: "<<reactionProps.size()<<"\n";
	cout<<"Reactants: "<<reactantsPopulations.size()<<"\n";

	thrust::device_vector<char> reactionOrder;//1,2,3
    thrust::device_vector<int> s0, s1;

    auto_ptr<CSRMatrixPacked> g_CSRMatrix;
	int reactantsCount=0, chsCounts=0;
	size_t propPitch=0, rctPopPitch=0;

	int *reactionToFirePos;
	PropSumReal_t *totalPropSum;
	PropReal_t *randomNumbersCurr, *randomNumbersNext;

	thrust::device_vector<int> rangeIndLimits;
	int longestList;
	try
    {
        InitDevice(reactionProps, reactantsPopulations, args, numLogIntervals, logStep,
			STAT_DIR, reactantsCount, chsCounts, longestList,
			reactionToFirePos, totalPropSum, randomNumbersCurr, randomNumbersNext);
	}catch(std::exception const &ec)
	{
		cerr<<ec.what()<<endl;
		return EXIT_FAILURE;
	}
	
	cout<<flush;

    cout<<"Initial structures are initialized, starting the algo"<<endl;

	//float prevStatisticTime=0;

	
	PropSumReal_t minTime=0.f;
	//
	AllPropensities(chsCounts);
	
	SortIndicesByPropDescAndPropRecompute(args.numParallel, chsCounts, g_rangesInArray, rangeIndLimits, totalPropSum, 0, minTime, longestList);
	
		
	InitRandomNumbersSet(args.numParallel, randomNumbersCurr);

	MyTime::Time_t tb=MyTime::CTime();
	double totalAvgInd=0;
	
	int it=0;
	for(; it<args.ai;++it){
		DEBUG_CHECK_CUDA_ERRORS();

				
		//we need some report from time to time about the execution's state,
		//but not too often as it will slow things down
		if(it%1000==0){
			minTime=MinTime();
			cout<<it<<" "<<minTime<<endl;
			if(minTime>=args.endTime){
				cout<<"Requested end time reached\n";
				break;
			}
		}

		RunBothKernels(args.numParallel, chsCounts, reactantsCount, 1, g_rangesInArray, false,
			totalPropSum, reactionToFirePos, g_elementsInChunk, randomNumbersCurr, raw_pointer_cast(&rangeIndLimits[0]), it, 1e-5f,
			collectStatistics);


		//if we need test main kernels in separation.

		//TestFindingKernel(args.numParallel, chsCounts, reactantsCount, loopsInKernel, g_rangesInArray, false,
		//	totalPropSum, reactionToFirePos, g_elementsInChunk, randomNumbersCurr, raw_pointer_cast(&rangeIndLimits[0]));
				

		//TestAdvancingKernel(args.numParallel, chsCounts, reactantsCount, loopsInKernel, false,
		//	totalPropSum, reactionToFirePos, g_elementsInChunk, randomNumbersCurr, randomNumbersNext,  
		//	raw_pointer_cast(&rangeIndLimits[0]), g_rangesInArray, it, 1e-6);

		DEBUG_CHECK_CUDA_ERRORS();
	}

	if(args.ai==it)
		cout<<"Maximum number of iterations reached\n";
	cudaThreadSynchronize();
	MyTime::Time_t te=MyTime::CTime();
	double elTime=MyTime::ElapsedTime(tb, te);
	cout<<elTime<<" ms for "<<it<<" iterations\n";

	//LogPropenseties();

	//avgInd/=(float)i;
	cudaThreadSynchronize();

	//float time=MyTime::ElapsedTime(ts, MyTime::CTime());
	//cout<<"Execution time="<<time<<"ms; "<</*i* */ loopsInKernel<<" iterations;\n";// performance: "<<i*args.numParallel*loopsInKernel/(time/1000.f)<<"it/s"<<endl;
	//cout<<"Average position of a reaction to be fired is: "<<avgInd<<"\n";

	if(collectStatistics)
		cummulativeStatistics(args.endTime, args.statPrefix);

	//ofstream stat("endStat.txt");
	//ShowAllPopulations(stat);
#ifndef NDEBUG
	system("PAUSE");
#endif

	//for (int i = 0; i < numberOfStreams; ++i) cudaStreamDestroy(stream[i]);
	g_affectedChannels.Free();

    return EXIT_SUCCESS;
   
}


