#include "functions.h"
#include "CudaPitched2D.h"
#include "CudaPitched3D.h"
#include "Statistics.h"
#include "propensities.h"
#include "finding.h"
 //#include <thrust/binary_search.h>
#include <thrust/scan.h>

#include <fstream>
#include <set>
#include "MyTime.h"

#include "defines.h"
#include "defines.cuh"

#include "d_print.h"

#include "MP_funcs.cuh"
#include "Network.h"

#include "const.h"
#include "CSCHostMatrixData.cuh"
#include <cassert>
#include <iomanip>
#include "realtype.h"

#include "AuxFunctors.cuh"

#include "ThreadRanges.h"

#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable: 4100 4512 4127)
#endif

#include <thrust/sort.h>
#include <thrust/scan.h>
#include <thrust/count.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/logical.h>
#include <thrust/binary_search.h>
#include <thrust/functional.h>
//#include <thrust/scan.h>

#define warpSize 32

#ifdef _MSC_VER
#pragma warning (pop)
#endif

#ifdef _MSC_VER
#pragma warning (disable: 4127)
#endif

//#include <boost/log/trivial.hpp>

using thrust::device_pointer_cast;
using thrust::counting_iterator;
using thrust::make_constant_iterator;
using thrust::device_ptr;
using thrust::host_vector;
using thrust::raw_pointer_cast;
using thrust::constant_iterator;
using std::cerr;
using std::cout;
using std::endl;
using std::ofstream;

using thrust::make_counting_iterator;
using thrust::raw_pointer_cast;

Network g_n;


// simple routine to print contents of a vector
template <typename Vector>
void print_vector(const std::string& name, const Vector& v)
{
  typedef typename Vector::value_type T;
  std::cout << "  " << std::setw(20) << name << "  ";
  thrust::copy(v.begin(), v.end(), std::ostream_iterator<T>(std::cout, " "));
  std::cout << std::endl;
}

thrust::device_vector<uint4> g_selectorSeeds;
thrust::device_vector<uint4> g_tauSeeds;
//thrust::device_vector<int> g_reactions2Fire;
thrust::device_vector<PropReal_t> g_times;

extern std::vector<std::ofstream*> g_logFiles;
extern bool statisticsEnabled;

Statistics g_Statistics;//not used if STATISTICS macro is not defined


AffectedChannels g_affectedChannels;


//textures, most likely, are not really helphul,
//but left "as is" from early stage of development
texture<PropReal_t, cudaTextureType1D> g_tPropensities;

#ifdef USE_TEXTURES

texture<int, cudaTextureType1D> ro_ChannelBegins;
texture<int, cudaTextureType1D> ro_ChannelReactants;

texture<float, cudaTextureType1D> ro_ChannelConstants;
texture<char, cudaTextureType1D>  ro_ChannelTypes;

#define GET_ELEM_FROM_RO_MEM(memPtrName, ind, offset)\
	tex1Dfetch(memPtrName, offset+ind)

#define REACTION_ROW_BEGIN(rn)\
	GET_ELEM_FROM_RO_MEM(ro_ChannelBegins, rn, 0)

#define GET_REACTANT(ri)\
	tex1Dfetch(ro_ChannelReactants, ri)

__device__ __forceinline__
int GetReactantInd(int ri)
{
	int value=GET_REACTANT(ri);
	int rExtInd; //internal (within a reaction) reactant's index, [0..3]
	char dummy;
	split(value, dummy, rExtInd);
	return rExtInd;
}

__device__ __forceinline__
char GetChannelType(int ch)
{
	return GET_ELEM_FROM_RO_MEM(ro_ChannelTypes, ch, 0);
}

//
__device__ __forceinline__
float ChannelConstant(int rn)
{
	return GET_ELEM_FROM_RO_MEM(ro_ChannelConstants, rn, 0);
}


#include "propfunctors.cuh"

void ChannelReactants(CSCHostMatrixData const &matrix, int channelsCount)
{
    thrust::host_vector<int> dummy(matrix.rowInd.size());
	cudaMalloc((void **)&g_n.d_ChannelReactants, matrix.rowInd.size()*sizeof(int));
	
	thrust::transform(matrix.rates.begin(), matrix.rates.end(), matrix.rowInd.begin(),
		dummy.begin(), ValueAndInd2Int());


	//sort reactants in each channel by increasing their orders
	for(int i=0; i<channelsCount; ++i)
	{
		int beginInd=matrix.colPtr[i];
		int chLen=matrix.colPtr[i+1]-beginInd;
		thrust::sort(&dummy[beginInd],&dummy[beginInd]+chLen, ChByRatesSorter());
	}

	thrust::copy(dummy.begin(), dummy.end(), device_pointer_cast(g_n.d_ChannelReactants));
}

#ifdef ODM

#ifdef CHANNELS_PER_REACTIONS_LIST

void MakeChannelsPerReactantsList(CSCHostMatrixData const &matrix, int channelsCount, int reactantsCount, 
	thrust::device_vector<short> &channelsPerReactantsList,
	thrust::device_vector<short> &reactantsPtr)
{
	
	assert(channelsCount==matrix.colPtr.size()-1);
	std::vector<std::set<short>> rctAssocChannels(reactantsCount);
	for(size_t i=0; i<matrix.colPtr.size()-1; ++i)
	{
		int bg=matrix.colPtr[i];
		int ed=matrix.colPtr[i+1];
		for(int ind=bg; ind<ed; ++ind)
		{
			int rctInd=matrix.rowInd[ind];
			rctAssocChannels[rctInd].insert(i);
		}
	}
}
#endif

#endif //ODM

#ifdef USE_TEXTURES

#define HOST_ARRAY_TO_TEXTURE(hArray, dArray, textureName, len, type)\
{\
	assert(dArray==NULL);\
	assert(hArray!=NULL);\
	assert(sizeof(type)==sizeof(*dArray));\
	assert(sizeof(*dArray)==sizeof(*dArray));\
	cudaMalloc((void **)&dArray, len*sizeof(type));\
	CHECK_CUDA_ERRORS();\
	\
	cudaMemcpy(dArray, hArray, len*sizeof(type), cudaMemcpyHostToDevice);\
	CHECK_CUDA_ERRORS();\
	\
	size_t offset;\
	cudaBindTexture(&offset, textureName, dArray, cudaCreateChannelDesc<type>());\
	CHECK_CUDA_ERRORS();\
	assert(offset==0);\
}

void DataToConstMemory(CSCHostMatrixData const &matrix, 
					   thrust::host_vector<float> &chConstants,
					   int reactantsCount)
{

	int channelsCount=chConstants.size();

	//populate channels' begins
	HOST_ARRAY_TO_TEXTURE(matrix.colPtr.data(), g_n.d_ChannelBegins, 
		ro_ChannelBegins, matrix.colPtr.size(), int);

	LogChannelBegins(matrix.colPtr.size());
		
	//populate channels' external and internal indices
	//need to pack rates and reactants indicies before copying to device
	//and bining to a texture
	{
		ChannelReactants(matrix, channelsCount);

		size_t offset;
		cudaBindTexture(&offset, ro_ChannelReactants, g_n.d_ChannelReactants, cudaCreateChannelDesc<int>());
		CHECK_CUDA_ERRORS();
		assert(offset==0);

	}

	LogReactantIndices(reactantsCount);

	//channels' constants
	HOST_ARRAY_TO_TEXTURE(chConstants.data(), g_n.d_ChannelConstants, 
		ro_ChannelConstants, channelsCount, float);

	LogChannelConstants(channelsCount);

	//channels' orders
	{

		cudaMalloc((void **)&g_n.d_ChannelTypes, channelsCount*sizeof(*g_n.d_ChannelTypes));
		
		CHECK_CUDA_ERRORS();
		counting_iterator<int> cit(0);
		thrust::transform(cit, cit+channelsCount,
			device_pointer_cast(g_n.d_ChannelTypes),
			ChannelOrderDetector());
		CHECK_CUDA_ERRORS();

		size_t offset;
		cudaBindTexture(&offset, ro_ChannelTypes, g_n.d_ChannelTypes, cudaCreateChannelDesc<char>());
		CHECK_CUDA_ERRORS();
		assert(offset==0);

	}

	LogChannelOrders(channelsCount);

}
#endif

CudaPitched2D<int> d_reactantPopulation;
CudaPitched2D<int> d_prevReactantPopulation;//used only for storing statistics
CudaPitched2D<PropReal_t> d_propensities;

__forceinline__
__device__ PropReal_t ReactionPropensity(int rn, int const *reactantPopulation)
{
	int const begin_ind=REACTION_ROW_BEGIN(rn);


	char chType=GetChannelType(rn);//GET_ELEM_FROM_RO_MEM(ro_ChannelTypes, rn, 0);

	
	int rExtInd=GET_REACTANT(begin_ind);
	char dummy;
	split(rExtInd, dummy, rExtInd);
	//assert(rIntInd==0);

	int x0=reactantPopulation[rExtInd];
	
	int x1=0;
	
	if(chType==2)
	{
		rExtInd=GET_REACTANT(begin_ind+1);
		split(rExtInd, dummy, rExtInd);
		x1=reactantPopulation[rExtInd];
	}

//	printf("rn=%d; chType=%d; x0=%d; x1=%d\n", rn, chType, x0, x1);
	
	PropReal_t p= Propensity(
		//GET_ELEM_FROM_RO_MEM(ro_ChannelConstants, rn, 0),
		ChannelConstant(rn),
		chType,
		x0, x1);
	return p;
}

__device__ float ReactionPropensity(int rn, int const *reactantPopulation, int const *chBegins, int const *chReactants, float const *chConstants, char const *channelType)
{
	int const begin_ind=chBegins[rn];

	char chType=channelType[rn];
	
	int rExtInd=chReactants[begin_ind];
	char dummy;
	split(rExtInd, dummy, rExtInd);
	//assert(rIntInd==0);

	int x0=reactantPopulation[rExtInd];
	
	int x1=0;
	
	if(chType==2)
	{ 
		rExtInd=chReactants[begin_ind+1];
		split(rExtInd, dummy, rExtInd);
		x1=reactantPopulation[rExtInd];
	}

	return Propensity(
		chConstants[rn],
		chType,
		x0, x1);
}


#ifdef ODM //functions that are only valid for ODM


#ifndef FIXED_CONFIG

__device__ void
PrintRctLen(int const *rctLen)
{
	for(int i=0; i<4; ++i)
	{
		for(int j=0; j<4; ++j)
			printf("%5d", rctLen[i*4+j]);
		printf("\n");
	}
}

__device__ inline void RangesToThreadsCount1(int idx, int totalLen, int *rctLen)
{
	if(idx>=16)
		return;

	int r=rctLen[idx];
	PropReal_t threadsPerRangeSum=(PropReal_t)r/totalLen*(warpSize-0.1f);
	rctLen[idx]=(int)threadsPerRangeSum;
	if(r!=0&&rctLen[idx]==0)
		rctLen[idx]=1;

#ifndef NDEBUG	
		int usedThreads=0;//check for not using more than THREADS_IN_WARP threads
		for(int i=0; i<4; ++i)
		{
			usedThreads+=rctLen[i*4+3];
		}
		if(idx==0)
			assert(usedThreads<=warpSize);
#endif
	
	
}
__device__ inline void rctLenPsByRanges(int idx, int *rctLen)
{
	if(idx>=4)
		return;
	for(int i=1; i<4; ++i)
	{
		rctLen[idx*4+i]+=rctLen[idx*4+i-1];
	}
}

__device__ inline int
Channels2Update (int idx, int const *rctLen)
{
	int res=0;
	for(int i=0; i<4; ++i)
	{
		res+=rctLen[i*4+3];
	}
	return res;
}

__device__ inline void
DetectRangeThreadEnds(int idx, int const *rctLen, int *rangeThreadEnds)
{
	if(idx>=4)
		return;
	rangeThreadEnds[idx]=rctLen[idx*4+3];

	if(idx==0)
	{
		for(int i=1; i<4; ++i)
		{
			rangeThreadEnds[i]+=rangeThreadEnds[i-1];
		}
		assert(rangeThreadEnds[3]<=warpSize);
	}
}

__device__ inline void
threadId2Ranges(int idx, int const *rctLen, int const *rangeThreadEnds,
				int *range, int *rnRange, int *inChunkIdx)
{
	*range=-1;
	*rnRange=-1;

	for(int i=0; i<4; ++i)
		if(idx<=rangeThreadEnds[i])
		{
			*range=i; break;
		}
	assert((*range)>=0&&(*range)<4);
	int const *rangeRow=&rctLen[(*range)*4];
	*inChunkIdx=(*range==0)?idx:(idx-rangeThreadEnds[*range-1]-1);//in range here
	for(int i=0; i<4; ++i)
		if((*inChunkIdx)<=rangeRow[i])
		{
			*rnRange=i; break;
		}

	if((*rnRange)!=0)
		*inChunkIdx-=rctLen[(*range)*4+(*rnRange)-1]+1;

}


#ifdef SORTED_INDICES_LIST

#endif //#ifdef SORTED_INDICES_LIST

#else

#endif

#ifdef SORTED_INDICES_LIST

#elif defined CHANNELS_PER_REACTIONS_LIST

__device__ void 
UpdatePropensitiesForReactionKernel(int idx,
									int rn, int reactantsCount, int channelsCount,
									int const *reactantPopulation, float *propensities,
									const short *channelsPerReactantsList, const short *reactantsPtr,
									int const* channelBegins, int const *channelReactants,
									float *channelConstants, char *channelTypes)
{
//#error not implemented
	if(idx>=4)
		return;

	int const rn_begin_ind=channelBegins[rn];
	int const rn_end_ind=channelBegins[rn+1];
	int i=rn_begin_ind+idx;
	if(i>=rn_end_ind)
		return;

	int ind=channelReactants[i];
	char order;
	split(ind, order, ind);
	assert(ind<reactantsCount);

	short chListBegin=reactantsPtr[ind];
	short chListEnd=reactantsPtr[ind+1];
	for(short chInd=chListBegin; chInd<chListEnd; ++chInd)
	{
		int affectedChannel=channelsPerReactantsList[chInd];
		assert(affectedChannel<channelsCount);
		propensities[affectedChannel]=ReactionPropensity(affectedChannel, reactantPopulation, channelBegins, channelReactants, channelConstants, channelTypes);
	}
	
}

#endif //#ifdef SORTED_INDICES_LIST/CHANNELS_PER_REACTIONS_LIST

#endif

__device__ inline
void ComputePropByMP(int idx, PropReal_t *rowProp, int const *rowReactsPopulation, int len)
{
	int chunkSize=DIV_UP(len, warpSize);

	int start=idx*chunkSize;
	int finish=(idx+1)*chunkSize;
	for(int i=start; i<finish; ++i)
	{
		if(i<len)
		{
			rowProp[i]=ReactionPropensity(i, rowReactsPopulation);
		}
	}
}

__global__ void AllPropensitiesKernel(int chsCount, int statesCount, 
									  CudaPitched2D<int> reactantPopulation, 
								 CudaPitched2D<PropReal_t> propensities)
{
	PROLOG;
	if(innerIdx>=chsCount||stateIdx>=statesCount)
		return;

	PropReal_t *rowProp=propensities.Row(stateIdx);
	int const *rowReactsPopulation=reactantPopulation.Row(stateIdx);
	ComputePropByMP(innerIdx, rowProp, rowReactsPopulation, chsCount);
}

__global__ void AllPropensitiesKernel2(int chsCount, int statesCount, 
									  CudaPitched2D<int> reactantPopulation, 
									  CudaPitched2D<PropReal_t> propensities)
{
	PROLOG;
	if(stateIdx>=statesCount)
		return;

	PropReal_t *rowProp=propensities.Row(stateIdx);
	int const *rowReactsPopulation=reactantPopulation.Row(stateIdx);
	ComputePropByMP(innerIdx, rowProp, rowReactsPopulation, chsCount);
}

#define KERNEL_CONFIG\
	int mult=1;\
	int const BLOCK_SIZE=32*mult;\
	int const BLOCKS=statesCount/mult;

#ifdef ODM
void AllPropensities(int chsCount)
{
	assert(d_reactantPopulation.Height()==d_propensities.Height());

	int statesCount=d_reactantPopulation.Height();

	KERNEL_CONFIG;
	AllPropensitiesKernel<<<BLOCKS, BLOCK_SIZE>>>(chsCount, statesCount, 
		d_reactantPopulation, d_propensities);
	
//	cout<<"Blocks:"<<BLOCKS<<" BLOCK_SIZE: "<<BLOCK_SIZE<<"\n";
	CHECK_CUDA_ERRORS();
}
#endif


void InitDeviceArrays(int chsCount, host_vector<int> const &rctsPopulation, int statesCount,
					  int numOfSteps, PropReal_t logStep, int elementsInChunk, bool storeStatistics)
{
	//cerr<<"here 1\n";
	d_reactantPopulation.Init(rctsPopulation.size(), statesCount);
	//cerr<<"here 2\n";
	d_prevReactantPopulation.Init(rctsPopulation.size(), statesCount);
	CHECK_CUDA_ERRORS();

	cudaError_t err=cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);
	if(err!=cudaSuccess){
		cerr<<"Cant set cache config\n";
		exit(EXIT_FAILURE);
	}

#ifdef ODM
	//cerr<<"here 3\n";
	d_propensities.Init(chsCount+31, statesCount);
	d_propensities.ZeroMatrix();
	CHECK_CUDA_ERRORS();
#endif


	g_times.resize(statesCount, 0.f);
	CHECK_CUDA_ERRORS();

	//distribute initial reactants' population accress multiple states
	for(int i=0; i<statesCount; ++i)
	{
		int* row = d_reactantPopulation.Row(i);
		thrust::copy(rctsPopulation.begin(), rctsPopulation.end(), device_pointer_cast(row));
	}
	CHECK_CUDA_ERRORS();

	//LogPopulation();
    if(storeStatistics){
		g_Statistics.Init(rctsPopulation.size(), statesCount, numOfSteps, logStep);
		CHECK_CUDA_ERRORS();
		g_Statistics.SetPopulation(d_reactantPopulation, 0);
	}
	
	CHECK_CUDA_ERRORS();
}

__host__ void CreateRandomSeedsUint4(int statesCount)
{
	host_vector<uint4> h_seeds;
	try
	{
		h_seeds.resize(statesCount);
		g_selectorSeeds.resize(statesCount);
		g_tauSeeds.resize(statesCount);
	}
	catch(std::exception const &ec)
	{
		fprintf(stderr, "%s while trying to allocate %d Mbytes of memory\n", 
			ec.what(), statesCount*sizeof(uint4)/(1024*1024));
		exit(EXIT_FAILURE);
	}
	
	if(!FillRandoms(static_cast<char *>(static_cast<void *>(&h_seeds[0])), 
		sizeof(uint4)/sizeof(char)*statesCount))
	{
		fprintf(stderr, "Can not allocate random seeds");
		exit(EXIT_FAILURE);
	}

	thrust::copy(h_seeds.begin(), h_seeds.end(), g_selectorSeeds.begin());

	if(!FillRandoms(static_cast<char *>(static_cast<void *>(&h_seeds[0])), 
		sizeof(uint4)/sizeof(char)*statesCount))
	{
		fprintf(stderr, "Can not allocate random seeds");
		exit(EXIT_FAILURE);
	}

	thrust::copy(h_seeds.begin(), h_seeds.end(), g_tauSeeds.begin());
	
}



__device__ inline PropReal_t Propensity(PropReal_t const *prpSum, int ind){
	return prpSum[ind+1]-prpSum[ind];
}


#ifdef ODM

#ifdef SHARED_ODM
#else

template <typename T>
__device__ __forceinline__
int FindInArray(int inWarpIdx, int warpInBlockIdx, T val2Search, PropReal_t const *arr, int arrLen, T volatile *s_aux_for_scan){
	int const chunks=WarpChunks(arrLen);
		
	__shared__ T add[WARPS_IN_BLOCK];

	if(inWarpIdx==31){
		add[warpInBlockIdx]=0.f;
	}

	s_aux_for_scan[inWarpIdx]=0.;

	for(int ch=0; ch<chunks; ++ch){
		T prpWarpSum=inclusive_warp_scan_alt(inWarpIdx, arr[ch*warpSize+inWarpIdx], s_aux_for_scan);

#ifdef  EXTRA_CHECK
		if(inWarpIdx==31)
		{
			T fsum=0;
			for(int i=0; i<32; ++i)
				fsum+=arr[ch*warpSize+i];
			if(fsum!=prpWarpSum)
				printf("Numerical trancation issue: %f and %f  shuld be equal\n", prpWarpSum, fsum);
		}

#endif
					
		int foundInChunkInd=FirstThreadIdThatGE(prpWarpSum+add[warpInBlockIdx], val2Search);//all threads in a warp get the same value

		if(foundInChunkInd>=0){
			return ch*warpSize+foundInChunkInd;//all threads return the same value
		}

		if(inWarpIdx==31){
			add[warpInBlockIdx]+=prpWarpSum;
		}
	}
	return -1;
}

//TODO: remove from here
template <typename T>
__device__ __forceinline__
int FindInArray(int inWarpIdx, int warpInBlockIdx, T val2Search, PropReal_t addition, PropReal_t const *arr, int arrLen, T volatile *s_aux_for_scan){
	int const chunks=WarpChunks(arrLen);
		
	__shared__ T add[WARPS_IN_BLOCK];

	if(inWarpIdx==31){
		add[warpInBlockIdx]=0.f;
	}

	s_aux_for_scan[inWarpIdx]=0.;

	for(int ch=0; ch<chunks; ++ch){
		T prpWarpSum=inclusive_warp_scan(inWarpIdx, arr[ch*warpSize+inWarpIdx], s_aux_for_scan);
			
		int foundInChunkInd=FirstThreadIdThatGE(prpWarpSum+add[warpInBlockIdx]+addition, val2Search);//all threads in a warp get the same value

		if(foundInChunkInd>=0){
			return ch*warpSize+foundInChunkInd;//all threads return the same value
		}

		if(inWarpIdx==31){
			add[warpInBlockIdx]+=prpWarpSum;
		}
	}
	return -1;
}


template <typename T>
__device__ inline
T SumOfArray(int inWarpIdx, int warpInBlockIdx, PropReal_t const *arr, int arrLen, T volatile *s_aux_for_scan){
	int const chunks=WarpChunks(arrLen);
//	int foundInChunkInd;
	int ch=0;
	__shared__ T add[WARPS_IN_BLOCK];

	if(inWarpIdx==31){
		add[warpInBlockIdx]=0.f;
	}

	s_aux_for_scan[inWarpIdx]=0.;

	for(; ch<chunks; ++ch){
		T propVal;
		if(ch*warpSize+inWarpIdx<arrLen)
			propVal=arr[ch*warpSize+inWarpIdx];
		else
			propVal=0;

		T prpWarpSum=inclusive_warp_scan(inWarpIdx, propVal, s_aux_for_scan);
	
		if(inWarpIdx==31){
			add[warpInBlockIdx]+=prpWarpSum;
		}
	}
	//printf("%f\n", (float)add[warpInBlockIdx]);
	return add[warpInBlockIdx];
}

template <typename T>
__device__ inline
void FireReaction(int inWarpIdx, int rtf, AffectedChannels affectedChannels, int *reactantPopulation, PropReal_t *prop, const int *newIndices, volatile T *chPropSum, volatile T *s_aux_for_scan)
{
	int rtfAffListLength=affectedChannels.RowLen(rtf);
	int const *row=affectedChannels.AffectedRow(rtf);


	T affChProp;
	if(inWarpIdx<rtfAffListLength){
			
		int affChInd=row[inWarpIdx];
		affChProp=ReactionPropensity(affChInd, reactantPopulation);
	}else
		affChProp=0;

	s_aux_for_scan[inWarpIdx]=0.;

	T scanOld=inclusive_warp_scan(inWarpIdx, affChProp, s_aux_for_scan);
	if(inWarpIdx==0){
		ReactantsFirer rf(reactantPopulation);
		rf(rtf);
	}
	if(inWarpIdx<rtfAffListLength){
		int affChInd=row[inWarpIdx];
		affChProp=ReactionPropensity(affChInd, reactantPopulation);
		prop[newIndices[affChInd]]=affChProp;//store new propensities
	}

	T scanNew=inclusive_warp_scan(inWarpIdx, affChProp, s_aux_for_scan);
		
	if(inWarpIdx==31){

		*chPropSum+=scanNew-scanOld;
		assert(chPropSum>=0);
	}


}


__device__ __forceinline__
int FindInPredictedChunk(int inWarpIdx, int predictedChunksCount, PropReal_t const *arr, PropReal_t val2Search, PropReal_t volatile *s_aux_for_scan, PropReal_t volatile *threadsPrefixScan)
{
	//assert(prevSum==s_aux_for_scan[63]);
	PropReal_t prevSum=s_aux_for_scan[63];

	PropReal_t threadSum=0;
	for(int i=0; i<predictedChunksCount; ++i){
		threadSum+=arr[warpSize*i+inWarpIdx];
	} 
	
	threadsPrefixScan[inWarpIdx]=inclusive_warp_scan(inWarpIdx, threadSum, s_aux_for_scan);

	int foundInThread=FirstThreadIdThatGE(threadsPrefixScan[inWarpIdx]+prevSum, val2Search);

	if(foundInThread==-1){//all threads in a warp return the same value!
		if(inWarpIdx==31)
			s_aux_for_scan[63]+=prevSum;
		return -1;
	}

	PropReal_t valInColumn=inWarpIdx<predictedChunksCount?arr[inWarpIdx*32+foundInThread]:0;
	PropReal_t scanedInColumn=inclusive_warp_scan(inWarpIdx, valInColumn, s_aux_for_scan);
	PropReal_t add=((foundInThread!=0)?threadsPrefixScan[foundInThread-1]:0)+prevSum;
	assert(add<val2Search);
	int rowInd=FirstThreadIdThatGE(scanedInColumn+add, val2Search);
	assert(rowInd!=-1);

	if(rowInd==-1)//all threads in a warp return the same value!
	{
		rowInd=predictedChunksCount-1;
	/*	if(inWarpIdx==predictedChunksCount-1)
			printf("%e %e %e\n",prevSum, scanedInColumn+add, valInColumn);*/
	}

#ifndef NDEBUG
	if(inWarpIdx==0){//TODO: remove this debugging staff
		PropReal_t sum=0;
		for(int i=0; i<=rowInd; ++i){
			sum+=arr[i*32+foundInThread];
		}
		assert(sum+add>=val2Search);
	}
#endif
	
	//if(inWarpIdx==0)
	//	printf("%d\n",rowInd);

	return rowInd*32+foundInThread;

}

__device__ __forceinline__
int FindInChunk(int inWarpIdx, PropReal_t const *arr, PropReal_t val2Search, int warpsInChunk, volatile PropReal_t *s_aux_for_scan, PropReal_t &add)
{
	add=s_aux_for_scan[63];
	volatile PropReal_t threadSum=0;
	for(int i=0; i<warpsInChunk/2; ++i){
		const int ind=warpSize*2*i+inWarpIdx;
		volatile PropReal_t val1=arr[ind];
		volatile PropReal_t val2=arr[ind+warpSize];
		threadSum+=val1+val2;
	} 

	//printf("%d, %f\n", inWarpIdx, threadSum);
	
	PropReal_t scan=inclusive_warp_scan(inWarpIdx, threadSum, s_aux_for_scan);

	//printf("%d, %f\n", inWarpIdx, scan);
	if(inWarpIdx==0)
		s_aux_for_scan[63]+=add;
	return FirstThreadIdThatGE(scan+add, val2Search);
}

__device__ __forceinline__
void FireReaction(int ch2Fire, int *population){
	
	int const beginInd=REACTION_ROW_BEGIN(ch2Fire);
	int const endInd=REACTION_ROW_BEGIN(ch2Fire+1);

	for(int i=beginInd; i<endInd; ++i)
	{
		int ind=GET_REACTANT(i);
		char order;
		split(ind, order, ind);
		if(population[ind]+order<0){//it might happen rarely due to floating point roinding errors while making partial propensities sum
			population[ind]=0;
		}else
			population[ind]+=order;
	}
}

__device__ __forceinline__
void FireReaction(int inWarpIdx, int ch2Fire, int *population){
	
	int const beginInd=REACTION_ROW_BEGIN(ch2Fire);
	int const endInd=REACTION_ROW_BEGIN(ch2Fire+1);

	int indToFire=beginInd+inWarpIdx;
	if(indToFire<endInd)
	{
		int ind=GET_REACTANT(indToFire);
		char order;
		split(ind, order, ind);
		if(population[ind]+order<0){//it might happen rarely due to floating point roinding errors while making partial propensities sum
			population[ind]=0;
		}else
			population[ind]+=order;
	}
}


struct UseInPartialSum{
	int m_maxIndex;

	__device__ 	UseInPartialSum(int maxIndex):m_maxIndex(maxIndex){}

	__device__ 
	bool operator()(int index)const{
		return index<m_maxIndex;
	}
};

void BindPropensities(CudaPitched2D<PropReal_t> const &propensities){
	CHECK_CUDA_ERRORS();
	size_t offset;
	cudaBindTexture(&offset, g_tPropensities, propensities.Row(0), cudaCreateChannelDesc<PropReal_t>());
	CHECK_CUDA_ERRORS();
}

__shared__ PropSumReal_t s_ext_shared[];

template <bool getsReactionNumber, bool saveStatistics, bool threadPerRange>
__global__ void AdvanceTimeAndUpdatePropensitiesByWarpIndirect(int realizationsCount, int chsCount, int rctsCount,
	uint4 * tauSeeds,
	PropReal_t *timeArray,
	PropReal_t *randomNumbers,
	int const* reactionToFire,
	CudaPitched2D<PropReal_t> propensities,
	CudaPitched2D<int> prevPopulation,
	CudaPitched2D<int> population,
	AffectedChannels affectedChannels,
	PropSumReal_t *totalPropSum, int const *rangesInd, int rangesInArray,
	Statistics statistics)
{

	int idx=blockDim.x * blockIdx.x + threadIdx.x;

	int const realizationIdx=idx>>5;

	if(realizationIdx>=realizationsCount)
		return;

	int const inWarpIdx=idx&31;
	
	int warpInBlockIdx=realizationIdx%WARPS_IN_BLOCK;

	__shared__ PropReal_t timeStep[WARPS_IN_BLOCK];
	uint4 seed;
	if(inWarpIdx==0){
		seed=tauSeeds[realizationIdx];
		timeStep[warpInBlockIdx]=-logf(HybridTausRngReduced(&seed))/totalPropSum[(realizationIdx+1)*rangesInArray-1];
		assert(timeStep[warpInBlockIdx]>0);
		//printf("timeStep: %e,  %f\n", timeStep[warpInBlockIdx], totalPropSum[(realizationIdx+1)*rangesInArray-1]);
	}

	int rnInd;
	if(getsReactionNumber)
		rnInd=reactionToFire[realizationIdx];
	else
		rnInd=(int)(randomNumbers[realizationIdx]*chsCount);

	//printf("Firing reaction %d %d\n", rnInd, realizationIdx);

	//if(inWarpIdx==0)
		//printf("Firing reaction %d %d\n", rnInd, realizationIdx);

	//
	if(rnInd<0||rnInd>chsCount){
#ifdef EXTRA_OUTPUT
		if(inWarpIdx==0)
			printf("wrong reaction-to-be-fired index occured: %d; realization: %d\n", rnInd, realizationIdx);
#endif
		return;
	}

	int rnToBeFired=rnInd;//initial index of a reaction

	
	assert(rnToBeFired>=0&&rnToBeFired<chsCount);

	int const *affectedReactions=affectedChannels.AffectedRow(rnToBeFired);
	int len=affectedChannels.RowLen(rnToBeFired);

	
	__shared__ PropSumReal_t s_aux_for_scan[64*WARPS_IN_BLOCK];
	(s_aux_for_scan+64*warpInBlockIdx)[inWarpIdx]=0;


	PropSumReal_t *oldPropSum=s_ext_shared;

	int const *threadRanges=affectedChannels.m_threadRanges+rnToBeFired*33;


	assert(threadRanges[inWarpIdx]<=threadRanges[inWarpIdx+1]);
	if(inWarpIdx>=rangesInArray)
		assert(threadRanges[inWarpIdx]==threadRanges[inWarpIdx+1]);

	
	//if(sum!=0)
	//	printf("tid: %d; sum: %f\n", inWarpIdx, sum);

	if(!threadPerRange){
		for(int i=0; i<rangesInArray; ++i)
			if(inWarpIdx==31)
				oldPropSum[warpInBlockIdx*rangesInArray+i]=0;

		assert(len<=warpSize);

		int chunks=DIV_UP(len, warpSize);
		for(int ch=0; ch<chunks; ++ch)
		{
			PropSumReal_t prop;
			int affctRn;
			if(inWarpIdx+ch*warpSize<len){
				affctRn=affectedReactions[inWarpIdx+ch*warpSize];
				prop=ReactionPropensity(affctRn, population.Row(realizationIdx));
			}else{
				prop=0;
				affctRn=chsCount+1;
			}

			for(int i=0; i<rangesInArray; ++i){
				UseInPartialSum predicate(rangesInd[i]);
				inclusive_warp_scan(inWarpIdx, prop, affctRn, predicate, s_aux_for_scan+64*warpInBlockIdx);
				if(inWarpIdx==31)
					oldPropSum[warpInBlockIdx*rangesInArray+i]+=(s_aux_for_scan+64*warpInBlockIdx)[63];
			}
		}
	}else
	{
		PropReal_t sum=0;
		for(int i=threadRanges[inWarpIdx]; i<threadRanges[inWarpIdx+1]; ++i){
			sum+=propensities.Row(realizationIdx)[affectedReactions[i]];
		}

		inclusive_warp_scan(inWarpIdx, sum, s_aux_for_scan+64*warpInBlockIdx);

		if(inWarpIdx<rangesInArray)
			oldPropSum[warpInBlockIdx*rangesInArray+inWarpIdx]=(s_aux_for_scan+64*warpInBlockIdx)[32+inWarpIdx];
	}

	__shared__ bool s_shouldSaveStatistics[WARPS_IN_BLOCK];
	if(saveStatistics){
		//printf("%f %f %f\n", timeArray[realizationIdx], timeStep[warpInBlockIdx], statistics.NextTime(realizationIdx));
		s_shouldSaveStatistics[warpInBlockIdx]=(timeArray[realizationIdx]+timeStep[warpInBlockIdx]>=statistics.NextTime(realizationIdx));
		if(s_shouldSaveStatistics[warpInBlockIdx]){
				//we need to store population on the previous time step to compute population in between two time steps
				CopyArrayByMP(inWarpIdx, prevPopulation.Row(realizationIdx), population.Row(realizationIdx), rctsCount);
		}
	}

	
	//fire with at most 4 threads used
	FireReaction(inWarpIdx, rnToBeFired, population.Row(realizationIdx));


	if(saveStatistics){
		if(s_shouldSaveStatistics[warpInBlockIdx]){ 
			PropReal_t timeStepRatio=(statistics.NextTime(realizationIdx)-timeArray[realizationIdx])/timeStep[warpInBlockIdx];
			statistics.StorePopulationMP(inWarpIdx, realizationIdx, prevPopulation.Row(realizationIdx), population.Row(realizationIdx), timeStepRatio);
		}
	}

	//affectedProp[inWarpIdx+32*warpInBlockIdx]=prop;

	PropSumReal_t *newPropSum=oldPropSum+rangesInArray*WARPS_IN_BLOCK;

	if(!threadPerRange){
		PropSumReal_t prop;
		int affctRn;

		for(int i=0; i<rangesInArray; ++i)
			if(inWarpIdx==31)
				newPropSum[warpInBlockIdx*rangesInArray+i]=0;

		assert(len<=warpSize);

		int chunks=DIV_UP(len, warpSize);
		for(int ch=0; ch<chunks; ++ch)
		{
			if(inWarpIdx+ch*warpSize<len){
				affctRn=affectedReactions[inWarpIdx+ch*warpSize];
				assert(affctRn>=0&&affctRn<chsCount);
		
				prop=ReactionPropensity(affctRn, population.Row(realizationIdx));
				//affctRn=backIndices[affctRn];

				propensities.Row(realizationIdx)[affctRn]=prop;
			}else{
				prop=0;
				affctRn=chsCount+1;
			}

			for(int i=0; i<rangesInArray; ++i){
				UseInPartialSum predicate(rangesInd[i]);
				inclusive_warp_scan(inWarpIdx, prop, affctRn, predicate, s_aux_for_scan+64*warpInBlockIdx);
				if(inWarpIdx==31)
					newPropSum[warpInBlockIdx*rangesInArray+i]=(s_aux_for_scan+64*warpInBlockIdx)[63];
			}
		}
	}else{

		PropReal_t sum=0;
		for(int i=threadRanges[inWarpIdx]; i<threadRanges[inWarpIdx+1]; ++i){
			PropSumReal_t prop=ReactionPropensity(affectedReactions[i], population.Row(realizationIdx));
			propensities.Row(realizationIdx)[affectedReactions[i]]=prop;
			sum+=prop;
		}

		inclusive_warp_scan(inWarpIdx, sum, s_aux_for_scan+64*warpInBlockIdx);

		if(inWarpIdx<rangesInArray)
			newPropSum[warpInBlockIdx*rangesInArray+inWarpIdx]=(s_aux_for_scan+64*warpInBlockIdx)[32+inWarpIdx];
	}

	if(inWarpIdx<rangesInArray){
		totalPropSum[realizationIdx*rangesInArray+inWarpIdx]+=newPropSum[warpInBlockIdx*rangesInArray+inWarpIdx]-oldPropSum[warpInBlockIdx*rangesInArray+inWarpIdx];
	}

	if(inWarpIdx==0){
		tauSeeds[realizationIdx]=seed;
		timeArray[realizationIdx]+=timeStep[warpInBlockIdx];
	}
}


struct RandomMaker{
	uint4 *m_seeds;
	PropReal_t *m_rn;
	RandomMaker(uint4 *seeds, PropReal_t *rn):m_seeds(seeds), m_rn(rn){}

	__device__ __inline__
	void operator()(int i)const{
		m_rn[i]=HybridTausRngReduced(&m_seeds[i]);
			
	}
};

void InitRandomNumbersSet(int realizationsCount, PropReal_t *randomNumbers){
	thrust::for_each(make_counting_iterator(0), make_counting_iterator(0)+realizationsCount, 
		RandomMaker(raw_pointer_cast(&g_selectorSeeds[0]), randomNumbers));
}

void CheckPropensities(int realization, int realizationsCount, int iteration, thrust::device_ptr<PropReal_t const> d_prop, thrust::device_ptr<PropReal_t> d_copyProp, int chsCount)
{
	if(!thrust::equal(d_prop, d_prop+chsCount, d_copyProp))
	{
		cout<<"iteration "<<iteration<<": bad propensity vector for realization "<<realization<<"\n";
		thrust::device_vector<PropReal_t> vp(d_prop, d_prop+chsCount);
		thrust::device_vector<PropReal_t> vcp(d_copyProp, d_copyProp+chsCount);
		print_vector("updated:", vp);
		print_vector("recomputed:", vcp);
	}
}

void CheckRanges(int rlzn, int iteration, int rangesInArray, 
	thrust::device_ptr<int const> d_rangesInd, 
	thrust::device_ptr<PropReal_t const> d_prop,
	thrust::device_ptr<PropSumReal_t const> d_propTotalSum, 
	float eps)
{
	thrust::device_vector<PropReal_t> ranges(rangesInArray);
	
	int prevRangeIndEnd=0;
	PropReal_t accSum=0;
	for(int r=0; r<rangesInArray; ++r){
		int rangeIndEnd=d_rangesInd[r];
		PropReal_t subRangeSum=thrust::reduce(d_prop+prevRangeIndEnd, d_prop+rangeIndEnd, (PropReal_t)0);
		accSum+=subRangeSum;

		PropReal_t updatedAccSum=d_propTotalSum[r];

		if(fabs(updatedAccSum-accSum)>eps)
		{
			cout<<"iteration "<<iteration<<"; realisztion: "<<rlzn<<" r="<<r<<"; expected: "<<accSum<<", computed: "<<updatedAccSum<<", diff: "<<fabs(updatedAccSum-accSum)<<"\n";
		}
		
		prevRangeIndEnd=rangeIndEnd;
	}
}

//might trigger false negative assertions when float type used for propensities
void CheckAll(int iteration, int realizationsCount, int chsCount, CudaPitched2D<PropReal_t> propCopy, 
	thrust::device_ptr<int const> d_rangesInd, int rangesInArray, thrust::device_ptr<PropSumReal_t const> d_propTotalSum,
	float eps)
{
	for(int i=0; i<realizationsCount; ++i){
		thrust::device_ptr<PropReal_t> d_prop=device_pointer_cast(d_propensities.Row(i));
		CheckPropensities(i, realizationsCount, iteration, d_prop, device_pointer_cast(propCopy.Row(i)), chsCount);
		CheckRanges(i, iteration, rangesInArray, d_rangesInd, d_prop, d_propTotalSum+rangesInArray*i, eps);
	}
}


//auxiliary function to run AdvanceTimeAndUpdatePropensitiesByWarpIndirect in separation
void TestAdvancingKernel(int realizationsCount, int chsCount, int rctsCount, int loops, bool extraCheck,
	PropSumReal_t *propTotalSum,
	int *reactionToFirePos, int predictedIndex, PropReal_t *&randomNumbersCurr, PropReal_t *&randomNumbersNext, 
	int const *rangesInd, int rangesInArray,
	int iteration, float eps,
	bool storeStatistics
	)
{
	int const BLOCK_SIZE=32*WARPS_IN_BLOCK;
	int const BLOCKS=DivUp(realizationsCount, WARPS_IN_BLOCK);

	thrust::device_ptr<int const> d_rangesInd=device_pointer_cast(rangesInd);
	thrust::device_ptr<PropSumReal_t> d_propTotalSum=device_pointer_cast(propTotalSum);

	thrust::for_each(make_counting_iterator(0), make_counting_iterator(0)+realizationsCount, 
		RandomMaker(raw_pointer_cast(&g_selectorSeeds[0]), randomNumbersNext));

	static CudaPitched2D<PropReal_t> propCopy;
	if(extraCheck){
		cerr<<"here 5\n";
		propCopy.Init(DivUp(chsCount,32)*32, realizationsCount);
	
		if(iteration==0){
			cout<<"Checking first iteration... ";
			AllPropensitiesKernel2<<<BLOCKS, BLOCK_SIZE>>>(chsCount, realizationsCount, 
				d_reactantPopulation, propCopy);

			CheckAll(iteration, realizationsCount, chsCount, propCopy, d_rangesInd, rangesInArray, d_propTotalSum, 
				eps*d_propTotalSum[rangesInArray-1]);
			cout<<"done\n";
		}
	}
	//MinTime();

	if(storeStatistics)
		AdvanceTimeAndUpdatePropensitiesByWarpIndirect<false, true, true><<<BLOCKS,BLOCK_SIZE, 2*(rangesInArray+1)*WARPS_IN_BLOCK*sizeof(PropSumReal_t)>>>(
			realizationsCount, chsCount, rctsCount,
			raw_pointer_cast(g_tauSeeds.data()),
			raw_pointer_cast(g_times.data()),
			randomNumbersCurr,
			reactionToFirePos,
			d_propensities,
			d_prevReactantPopulation,
			d_reactantPopulation,
			g_affectedChannels,
			propTotalSum,
			rangesInd, rangesInArray,
			g_Statistics);
	else
		AdvanceTimeAndUpdatePropensitiesByWarpIndirect<false, false, true><<<BLOCKS,BLOCK_SIZE, 2*(rangesInArray+1)*WARPS_IN_BLOCK*sizeof(PropSumReal_t)>>>(
			realizationsCount, chsCount, rctsCount,
			raw_pointer_cast(g_tauSeeds.data()),
			raw_pointer_cast(g_times.data()),
			randomNumbersCurr,
			reactionToFirePos,
			d_propensities,
			d_prevReactantPopulation,
			d_reactantPopulation,
			g_affectedChannels,
			propTotalSum,
			rangesInd, rangesInArray,
			g_Statistics);


	//MinTime();

	//thrust::device_vector<double> scan(chsCount);

	if(extraCheck){
		AllPropensitiesKernel2<<<BLOCKS, BLOCK_SIZE>>>(chsCount, realizationsCount, 
				d_reactantPopulation, propCopy);

		PropReal_t totalPropSum=d_propTotalSum[rangesInArray-1];
		CheckAll(iteration, realizationsCount, chsCount, propCopy, d_rangesInd, rangesInArray, d_propTotalSum, eps*totalPropSum);
			
		//cout<<"total prop sum is: "<<totalPropSum<<"\n";

		propCopy.Destroy();
	}

	//cudaThreadSynchronize();
	std::swap(randomNumbersCurr, randomNumbersNext);

	if(extraCheck){
		cudaError_t err=cudaGetLastError();
		if(err!=cudaSuccess){
			if(err==cudaErrorInvalidConfiguration){
				cerr<<"Wrong main kenrel configuration:\n"<<
					"\t"<<BLOCKS<<"x"<<BLOCK_SIZE<<"\n";
			}else{
				cerr<<"Kernel failed with code "<<err<<"\n";
			}
			exit(EXIT_FAILURE); 
		}
	}
}


struct AbnormalReaction{
	int m_chsCount;
	AbnormalReaction(int chsCount):m_chsCount(chsCount){}

	__device__ inline
	bool operator()(int rnInd)const{
		return rnInd<0||rnInd>=m_chsCount;
	}
};


//auxiliary function to run FindReactionToBeFired in separation
void TestFindingKernel(int realizationsCount, int chsCount, int rctsCount, int loops, int rangesInArray, bool extraCheck,
	PropSumReal_t *propTotalSum,
	int *reactionToFirePos, int predictedIndex, PropReal_t *randomNumbers, int const *rangesIndices
	)
{
	int const BLOCK_SIZE=32*WARPS_IN_BLOCK;
	int const BLOCKS=DivUp(realizationsCount, WARPS_IN_BLOCK);

	FindReactionToBeFired(realizationsCount, chsCount, raw_pointer_cast(g_selectorSeeds.data()), 
		d_propensities, propTotalSum, predictedIndex, rangesIndices, rangesInArray, reactionToFirePos, randomNumbers, true);

	

	if(extraCheck){

		thrust::device_vector<PropReal_t> scan(chsCount);

		for(int i=0; i<realizationsCount; ++i){
			int foundInd=device_pointer_cast(reactionToFirePos)[i];
			PropReal_t val2Find=device_pointer_cast(randomNumbers)[i];

			thrust::device_ptr<PropReal_t> d_prop=device_pointer_cast(d_propensities.Row(i));
			thrust::copy(d_prop, d_prop+chsCount, scan.begin());
			//print_vector("propensities:", scan);
			thrust::inclusive_scan(scan.begin(), scan.end(), scan.begin());
			auto it=thrust::upper_bound(scan.begin(), scan.end(), val2Find);
			int foundInd2=thrust::distance(scan.begin(), it);
		//	cout<<foundInd<<" "<<foundInd2<<"\n";

			if(foundInd!=foundInd2)
			{
				assert(foundInd<chsCount);
				thrust::device_ptr<const int> d_rI=device_pointer_cast(rangesIndices);
				auto it=thrust::upper_bound(d_rI, d_rI+rangesInArray, foundInd2);
				cout<<"\twas looking for: "<<(PropReal_t)device_pointer_cast(randomNumbers)[i]
					<<" scanMy["<<foundInd<<"]="<<scan[foundInd]<<" scanThrust["<<foundInd2<<"]="<<scan[foundInd2]<<" propMy: "<<d_prop[foundInd]<<" propThrust: "<<d_prop[foundInd2]<<"\n";
				//print_vector("scan:", scan);
			}
		}


	
		int badRnCount=thrust::count_if(device_pointer_cast(reactionToFirePos), device_pointer_cast(reactionToFirePos)+realizationsCount, AbnormalReaction(chsCount));
		if(badRnCount!=0)
			cerr<<badRnCount<<" bad reaction numbers found\n";
		cudaError_t err=cudaGetLastError();
		if(err!=cudaSuccess){
			if(err==cudaErrorInvalidConfiguration){
				cerr<<"Wrong main kenrel configuration:\n"<<
					"\t"<<BLOCKS<<"x"<<BLOCK_SIZE<<"\n";
			}else{
				cerr<<"Kernel failed with code "<<err<<"\n";
			}
			exit(EXIT_FAILURE); 
		}
	}
}

struct ValidRN{
	int m_chsCount;

	explicit ValidRN(int chsCount):m_chsCount(chsCount){}

	__device__ inline
	bool operator()(int ind){
		return ind>=0&&ind<m_chsCount;
	}
};

void RunBothKernels(int realizationsCount, int chsCount, int rctsCount, int loops, int rangesInArray, bool extraCheck,
	PropSumReal_t *propTotalSum,
	int *reactionToFirePos, int predictedIndex, PropReal_t *randomNumbers, int const *rangesIndices,
	int iteration, float eps,
	bool storeStatistics
	)
{
	int const BLOCK_SIZE=32*WARPS_IN_BLOCK;
	int const BLOCKS=DivUp(realizationsCount, WARPS_IN_BLOCK);

	FindReactionToBeFired(realizationsCount, chsCount, raw_pointer_cast(g_selectorSeeds.data()), 
		d_propensities, propTotalSum, predictedIndex, rangesIndices, rangesInArray, reactionToFirePos, randomNumbers, false);


	const bool getsReactionNumber=true;//can be assigned false for testing purposes, to advance random reaction
	const bool threadPerRange=true;//"false" variant might need some polishing

	if(storeStatistics)
		AdvanceTimeAndUpdatePropensitiesByWarpIndirect<getsReactionNumber, true, threadPerRange><<<BLOCKS, BLOCK_SIZE, 2*rangesInArray*WARPS_IN_BLOCK*sizeof(PropSumReal_t)>>>(
			realizationsCount, chsCount, rctsCount,
			raw_pointer_cast(g_tauSeeds.data()),
			raw_pointer_cast(g_times.data()),
			randomNumbers,
			reactionToFirePos,
			d_propensities,
			d_prevReactantPopulation,
			d_reactantPopulation,
			g_affectedChannels,
			propTotalSum, rangesIndices, rangesInArray,
			g_Statistics);
	else
		AdvanceTimeAndUpdatePropensitiesByWarpIndirect<getsReactionNumber, false, threadPerRange><<<BLOCKS, BLOCK_SIZE, 2*rangesInArray*WARPS_IN_BLOCK*sizeof(PropSumReal_t)>>>(
			realizationsCount, chsCount, rctsCount,
			raw_pointer_cast(g_tauSeeds.data()),
			raw_pointer_cast(g_times.data()),
			randomNumbers,
			reactionToFirePos,
			d_propensities,
			d_prevReactantPopulation,
			d_reactantPopulation,
			g_affectedChannels,
			propTotalSum, rangesIndices, rangesInArray,
			g_Statistics);


	if(extraCheck){
		int badRnCount=thrust::count_if(device_pointer_cast(reactionToFirePos), device_pointer_cast(reactionToFirePos)+realizationsCount, AbnormalReaction(chsCount));
		if(badRnCount!=0)
			cerr<<badRnCount<<" bad reaction numbers found\n";
		cudaError_t err=cudaGetLastError();
		if(err!=cudaSuccess){
			if(err==cudaErrorInvalidConfiguration){
				cerr<<"Wrong main kenrel configuration:\n"<<
					"\t"<<BLOCKS<<"x"<<BLOCK_SIZE<<"\n";
			}else{
				cerr<<"Kernel failed with code "<<err<<"\n";
			}
			exit(EXIT_FAILURE); 
		}
	}

}



#endif



struct Count2Key:public thrust::unary_function<int, int>
{
	int m_statesCount;
	Count2Key(int statesCount):m_statesCount(statesCount){}

	__device__ inline
	int operator()(int count)const
	{
		return count/m_statesCount;
	}
};


struct IsNormalProp:public thrust::unary_function<bool, PropReal_t>
{ 
	IsNormalProp(){}

	__device__ inline
	int operator()(PropReal_t prop)const
	{
		return prop>=0&&prop<1e+12;
	}

};


__global__ void TotalSumKernel(int chsCount, int statesCount, 
									  CudaPitched2D<PropReal_t> propensities,
									  int rangeCounts,
									  int *rangeIndLimits,
									  PropSumReal_t *totalPropSum)
{
	PROLOG;
	if(stateIdx>=statesCount)
		return;

	PropReal_t *rowProp=propensities.Row(stateIdx);
	__shared__ double s_aux_for_scan[WARPS_IN_BLOCK*64];

	int warpInBlockIdx=stateIdx%WARPS_IN_BLOCK;
	
	PropReal_t totalSum=0.f;
	for(int i=0; i<rangeCounts; ++i){
		PropReal_t *propRangeStart;
		int len;
		if(i==0){
			propRangeStart=rowProp;
			len=rangeIndLimits[i];
		}
		else{
			propRangeStart=rowProp+rangeIndLimits[i-1];
			len=rangeIndLimits[i]-rangeIndLimits[i-1];
		}


		totalSum+=SumOfArray(innerIdx, warpInBlockIdx, propRangeStart, len, s_aux_for_scan+warpInBlockIdx*64);

		if(innerIdx==31){
			totalPropSum[stateIdx*rangeCounts+i]=totalSum;
		}
	}
}


struct PropSorter
{
	PropReal_t const *m_prop;
	int const *m_revInd;
	explicit PropSorter(PropReal_t const *prop, int const *revInd):m_prop(prop), m_revInd(revInd){}

	__device__ inline
	bool operator()(int chIndexl, int chIndexr)const{
		return m_prop[m_revInd[chIndexl]]>m_prop[m_revInd[chIndexr]];
	}
};

struct RevIndexFn
{
	int *m_revIndices;
	int const *m_directIndices;
	explicit RevIndexFn(int const *directIndices, int *revIndices):m_directIndices(directIndices), m_revIndices(revIndices){}

	__device__ inline
	void operator()(int ind)const{
		int revInd=m_directIndices[ind];
	//	assert(revInd>=0&&revInd<m_chsCount);
		m_revIndices[revInd]=ind;
	}
};

//split propensity array into subranges to have equal probabilities to hit every subrange (biased to the beginning)
//seems to be faster
void DetectRangesProbUniform(thrust::device_vector<int> &elemsInRange, int rangesInArray, int channelsCount, PropReal_t totalPropSum, thrust::device_vector<PropReal_t> const &scan){
	thrust::device_vector<PropReal_t> limits;
	elemsInRange.resize(rangesInArray);

	for(int i=1; i<=rangesInArray; ++i){
		limits.push_back((1.f/(PropReal_t)rangesInArray)*i*totalPropSum);
	}

	thrust::upper_bound(scan.begin(), scan.end(),
		limits.begin(), limits.end(),
		elemsInRange.begin());

	//we might have some reactions with 0 propensities. 
	//These reactions were not included in the last subrange so we need to modify it manually
	elemsInRange.back()=channelsCount;
	
	print_vector("limits", limits);
}

//split propensity array into subranges to have equal number of elements in every subrange
void DetectRangesElemsUniform(thrust::device_vector<int> &elemsInRange, int rangesInArray, int channelsCount){
	for(int i=1; i<=rangesInArray; ++i){
		elemsInRange.push_back((int)((1.f/(PropReal_t)rangesInArray)*i*channelsCount));
	}
}


//this must be called at least once, before computation starts
//the idea was to resort reactions once in a while,
// but now it is just used for initialization
void SortIndicesByPropDescAndPropRecompute(int realizationsCount, int channelsCount, int rangesInArray, 
	thrust::device_vector<int> &elemsInRange,
	PropSumReal_t *totalPropSum,
	int iterationMade, PropReal_t minTime, 
	int maxListLen)
{
	CHECK_CUDA_ERRORS();

	cout<<"resorting reactions... ";
	

	int const BLOCK_SIZE=32*WARPS_IN_BLOCK;
	int const BLOCKS=DivUp(realizationsCount, WARPS_IN_BLOCK);
		
	int rInd=rand()/(PropReal_t)(RAND_MAX+1)*realizationsCount;


	PropReal_t oldPropSum=device_pointer_cast(totalPropSum)[rInd*rangesInArray+rangesInArray-1];
	

	AllPropensitiesKernel2<<<BLOCKS, BLOCK_SIZE>>>(channelsCount, realizationsCount, d_reactantPopulation, d_propensities);

	{
		thrust::device_vector<PropReal_t> scan(channelsCount);
		thrust::inclusive_scan(device_pointer_cast(d_propensities.Row(rInd)), device_pointer_cast(d_propensities.Row(rInd))+channelsCount,
			scan.begin());

		PropReal_t totalPropSum=scan.back();
		cout<<"total prop sum is: "<<totalPropSum<<"\n";

		DetectRangesElemsUniform(elemsInRange, rangesInArray, channelsCount);
		//DetectRangesProbUniform(elemsInRange, rangesInArray, channelsCount, totalPropSum, scan);
		print_vector("ranges' ends", elemsInRange);
				
	}

	

	DetectThreadRanges(g_affectedChannels, channelsCount, elemsInRange, rangesInArray, maxListLen);
	
	
	thrust::device_vector<PropReal_t> aux(channelsCount);
	thrust::copy(device_pointer_cast(d_propensities.Row(rInd)), device_pointer_cast(d_propensities.Row(rInd))+channelsCount, aux.begin());

	TotalSumKernel<<<BLOCKS, BLOCK_SIZE>>>(channelsCount, realizationsCount, d_propensities, 
		rangesInArray, raw_pointer_cast(&elemsInRange[0]), totalPropSum);

	PropReal_t newPropSum=device_pointer_cast(totalPropSum)[rInd*rangesInArray+rangesInArray-1];
	
	cout<<iterationMade<<" iterations made; min time="<<minTime<<"; old total sum: "<<oldPropSum<<" new total sum: "<<newPropSum<<" done\n";

	CHECK_CUDA_ERRORS();
}


void InitSortedIndicesAndPropSum(int realizationsCount, int channelsCount, int rangesInArray,
	int *&reactionToFire, PropSumReal_t *&propSums,
	PropReal_t *&randomNumbersCurr, PropReal_t *&randomNumbersNext 
	//, PropReal_t *&additions
	//, int *&foundInRange
	)
{
	CHECK_CUDA_ERRORS();

	cudaMalloc((void**)&reactionToFire, sizeof(int)*realizationsCount);
		
	cudaMalloc((void**)&propSums, sizeof(PropSumReal_t)*realizationsCount*rangesInArray);
	cudaMalloc((void **)&randomNumbersCurr, sizeof(PropReal_t)*realizationsCount);
	cudaMalloc((void **)&randomNumbersNext, sizeof(PropReal_t)*realizationsCount);

	auto d_p=device_pointer_cast(d_propensities.Row(0));
	PropReal_t pSum=thrust::reduce( d_p, d_p+channelsCount, 0.f);

	thrust::copy(make_constant_iterator(pSum), make_constant_iterator(pSum)+realizationsCount, device_pointer_cast(propSums));

	CHECK_CUDA_ERRORS();
}


#endif //SHARED_ODM

#else//ODM

#endif


PropReal_t MinTime()
{
	return *thrust::min_element(g_times.begin(),g_times.end());
	
}

int StatesReachedTime(PropReal_t time)
{
	return thrust::count_if(g_times.begin(), g_times.end(), GEThan(time));
}

class MinMaxEr: public thrust::binary_function<int,int,bool>
{
	int m_rI;
	CudaPitched2D<int> m_rP;
public:
	MinMaxEr(CudaPitched2D<int> rP, int rI):m_rI(rI), m_rP(rP){}
	__device__ inline 
	bool operator()(int s1, int s2)const 
	{
		int const *row1=m_rP.Row(s1);
		int const *row2=m_rP.Row(s2);

		return row1[m_rI]<row2[m_rI];
	}
};

class Reducer: public thrust::unary_function<int,bool>
{
	int m_rI;
	CudaPitched2D<int> m_rP;
public:
	Reducer(CudaPitched2D<int> rP, int rI):m_rI(rI), m_rP(rP){}
	__device__ inline 
	size_t operator()(size_t s)const 
	{
		int const *row=m_rP.Row(s);
		
		return row[m_rI];
	}
};

void ShowAllPopulations(std::ostream &os)
{
	
	for(int i=0; i<g_Statistics.Steps(); ++i)
	{
		os<<endl<<"step "<<i<<endl;
		for(int sysId=0; sysId<g_Statistics.Systems(); ++sysId)
		{
			device_ptr<PropReal_t const> arr=device_pointer_cast(g_Statistics.Population(sysId, i));
			os<<DevArrStreamer(arr, g_Statistics.Reactants())<<endl;
		}
		//int pp=device_ptr<float>(g_Statistics.Times())[i];
		//cout<<pp<<endl;
	}

}

void LogStatistics(int logStepsMade, int it, PropReal_t time, int realizationsCount, PropSumReal_t const *totalPropSum)
{
	cout<<logStepsMade<<", "<<it<<" iterations made. logging for time "<<time<<"\n";

//#ifndef NDEBUG
	/*PropSumReal_t res=thrust::reduce(device_pointer_cast(totalPropSum), device_pointer_cast(totalPropSum)+realizationsCount, 0.f)/(PropSumReal_t)realizationsCount;
	cout<<"; average total propensities sum is: "<<res;
//#endif
	cout<<endl;*/

	/*if(!statisticsEnabled) 
		return;

	int statesCount=d_reactantPopulation.Height();

	for(size_t i=0; i<g_logFiles.size(); ++i)
	{
		if(!g_logFiles[i]->is_open())
			return;

		//TODO: reimplement on thrust
		thrust::counting_iterator<int> cit(0);
		thrust::pair<thrust::counting_iterator<int>, thrust::counting_iterator<int> > resNX=
			thrust::detail::dispatch::minmax_element(cit, 
				cit+statesCount,MinMaxEr(d_reactantPopulation, i),thrust::device_space_tag());
		//int mn=std::numeric_limits<int>::max();
		//int mx=-mn;

		thrust::counting_iterator<size_t> cit_s(0);
		size_t sum=thrust::transform_reduce(cit_s, 
				cit_s+statesCount,
				Reducer(d_reactantPopulation, i),
				0,
				thrust::plus<size_t>());

		
		//size_t avg=0;
		//for(int s=0; s<statesCount; ++s)
		//{
		//	device_ptr<int> statePopulation=device_pointer_cast(d_reactantPopulation.Row(s));
		//	int popl=statePopulation[i];
		//	//mn=std::min(mn, popl);
		//	//mx=std::max(mx, popl);
		//	avg+=popl;
		//}

		//assert(avg==sum);

		int const *minRow=d_reactantPopulation.Row(*resNX.first);
		int const *maxRow=d_reactantPopulation.Row(*resNX.second);

		int mn=device_pointer_cast(minRow)[i];
		int mx=device_pointer_cast(maxRow)[i];
		float fAvg=sum/(float)statesCount;
		//float min=thrust::min_element(d_reactantPopulation.Row(i));
		*g_logFiles[i]<<step<<"\t"<<time<<"\t"<<mn<<"\t"<<fAvg<<"\t"<<mx<<endl;
	}
*/
	
}

void cummulativeStatistics(float totalTime, std::string statPrefix)
{
	assert(!statPrefix.empty());
	std::vector<std::vector<CummStat>> cummStat;
	g_Statistics.Compute(cummStat);

	print(cummStat, statPrefix.c_str(), totalTime);

}

#include "Loggers.cuh"