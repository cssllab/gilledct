#include "aux_funcs.h"

#include "CSCHostMatrixData.h"
#include "AffectedChannels.h"
#include <cassert>
#include <vector>
#include <set>
#include <thrust/extrema.h>

using std::cout;
using thrust::device_pointer_cast;


void MakeAssocChannelsList(CSCHostMatrixData const &matrix, int channelsCount, int reactantsCount, AffectedChannels &affectedChannels, int &longestList)
{
	assert(channelsCount==matrix.colPtr.size()-1);
	std::vector<std::set<int>> rctAssocChannels(reactantsCount);
	//cout<<"a 1\n";
	for(size_t i=0; i<matrix.colPtr.size()-1; ++i)
	{
		int bg=matrix.colPtr[i];
		int ed=matrix.colPtr[i+1];
		for(int ind=bg; ind<ed; ++ind)
		{
			int rctInd=matrix.rowInd[ind];
			if(matrix.rates[ind]<0)//store only reactions where reactant is on the left side (not a product) in the reaction
				rctAssocChannels[rctInd].insert(i);
		}
	}

	//cout<<"a 2\n";
	std::vector<std::set<int>> chsAssocChannels(channelsCount);
	for(int i=0; i<channelsCount; ++i)
	{
		int bg=matrix.colPtr[i];
		int ed=matrix.colPtr[i+1];
		std::set<int> &currChsAssocChannels=chsAssocChannels[i];
		for(int ind=bg; ind<ed; ++ind)
		{
			int rctInd=matrix.rowInd[ind];
			currChsAssocChannels.insert(rctAssocChannels[rctInd].begin(), rctAssocChannels[rctInd].end());
		}
		currChsAssocChannels.insert(i);//must be there anyway...
	}

	//cout<<"a 3\n";
	std::vector<std::set<int>>().swap(rctAssocChannels);

	thrust::host_vector<int> rowsStarts(channelsCount+1);
	thrust::host_vector<int> assocChannels;
	thrust::host_vector<int> segments;

	int currRowStart=0;
	longestList=0;
	for(int i=0; i<channelsCount; ++i)
	{
		rowsStarts[i]=currRowStart;
		std::set<int> const &currChsAssocChannels=chsAssocChannels[i];
		
		assocChannels.resize(assocChannels.size()+currChsAssocChannels.size());
		segments.resize(segments.size()+currChsAssocChannels.size());
		std::copy(currChsAssocChannels.begin(), currChsAssocChannels.end(), assocChannels.begin()+currRowStart);
		std::fill(segments.begin()+currRowStart, segments.begin()+currRowStart+currChsAssocChannels.size(), i);
		currRowStart+=currChsAssocChannels.size();
		longestList=std::max(longestList,(int)currChsAssocChannels.size());
	}
	rowsStarts[channelsCount]=currRowStart;
	assert(currRowStart==assocChannels.size());
	cout<<"total length of affected matrix is: "<<currRowStart<<", longest list: "<<longestList<<"\n";

	
	affectedChannels.Init(channelsCount, currRowStart);
		
	cudaMemcpy(affectedChannels.m_rowStarts, &rowsStarts[0], (channelsCount+1)*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(affectedChannels.m_affcetedLists, &assocChannels[0], currRowStart*sizeof(int), cudaMemcpyHostToDevice);
	
	
	auto it=thrust::minmax_element(device_pointer_cast(affectedChannels.m_affcetedLists), device_pointer_cast(affectedChannels.m_affcetedLists)+currRowStart);
	assert(*it.first>=0);
	assert(*it.second<channelsCount);

	CHECK_CUDA_ERRORS();
}